#!/usr/bin/env bash

set -e
cd $(dirname $0)

pip-compile -U requirements.in -o requirements.txt

cd -