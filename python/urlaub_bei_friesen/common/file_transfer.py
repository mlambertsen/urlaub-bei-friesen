import os

from paramiko import SSHClient
from scp import SCPClient
from typing import List


def upload_files(
    ssh_client: SSHClient,
    file_paths: List[str],
    target_root_path: str,
    local_path_prefix: str,
):
    with SCPClient(ssh_client.get_transport()) as scp:
        for file in file_paths:
            target_path = os.path.join(target_root_path, file)
            target_dir = os.path.dirname(target_path)
            ssh_client.exec_command(f"mkdir -p {target_dir}")
            scp.put(
                files=str(os.path.join(local_path_prefix, file)),
                remote_path=str(target_path),
            )
    print(
        f"Uploaded files {file_paths} to remote:{target_root_path}"
        f" maintaining directory structure."
    )


def download_files(ssh_client: SSHClient, src_paths: List[str], target_root_path: str):
    with SCPClient(ssh_client.get_transport()) as scp:
        for src_path in src_paths:
            os.makedirs(
                os.path.join(target_root_path, os.path.dirname(src_path)), exist_ok=True
            )
            target_path = os.path.join(target_root_path, src_path)
            print(f"Download file from remote:{src_path} to localhost:{target_path}.")
            scp.get(src_path, target_path)
