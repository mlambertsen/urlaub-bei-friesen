OCCUPANCY_LEFT_FILE_PATH = "anfrage/config/belegung_links.txt"
OCCUPANCY_RIGHT_FILE_PATH = "anfrage/config/belegung_rechts.txt"

PRICES_FILE_PATH = "preise_und_angebote/config/prices.json"
PRICES_MAIN_SEASON_FILE_PATH = "preise_und_angebote/config/main_season.txt"
PRICES_VALIDITY_FILE_PATH = "preise_und_angebote/config/validity.txt"

CONFIG_FILES = [
    OCCUPANCY_LEFT_FILE_PATH,
    OCCUPANCY_RIGHT_FILE_PATH,
    PRICES_FILE_PATH,
    PRICES_MAIN_SEASON_FILE_PATH,
    PRICES_VALIDITY_FILE_PATH,
]
