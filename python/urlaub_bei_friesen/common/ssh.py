from getpass import getpass
from paramiko import SSHClient, AutoAddPolicy


DEFAULT_HOST = "ssh.strato.de"
DEFAULT_USER = "xn--fhr-fewo-n4a.de"


def connect(host: str = None, user: str = None, password: str = None):
    if not host:
        host = input("SSH Host: ")
    if not user:
        user = input("SSH User: ")
    if not password:
        password = getpass("Password for ssh connection: ")
    ssh = SSHClient()
    ssh.set_missing_host_key_policy(AutoAddPolicy())
    ssh.connect(hostname=host, username=user, password=password)
    return ssh
