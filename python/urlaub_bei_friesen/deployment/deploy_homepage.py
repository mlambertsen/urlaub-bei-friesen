import argparse
import git
import os
import subprocess
import textwrap

from datetime import datetime
from pathlib import Path
from typing import Dict, List

import urlaub_bei_friesen.common.config_files as config_files
import urlaub_bei_friesen.common.file_transfer as file_transfer
from urlaub_bei_friesen.common.path import GIT_REPO_PATH
import urlaub_bei_friesen.common.ssh as ssh

TARGETS = {
    "production": ".",
    "staging": "staging",
}


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-t",
        "--target",
        help="Target environment for the deployment",
        type=str,
        required=True,
        choices=TARGETS.keys(),
    )
    parser.add_argument(
        "-c",
        "--config",
        help="Update also config files",
        action="store_true",
    )
    return parser.parse_args()


def create_find_command(directory: Path) -> List[str]:
    return ["find", str(directory), "-type", "f", "-exec", "sha256sum", "{}", "+"]


def create_remote_checksum_line_list(ssh_client, directory: Path):
    _, ssh_out, _ = ssh_client.exec_command(" ".join(create_find_command(directory)))
    return [line for line in ssh_out.readlines()]


def create_local_checksum_line_list(directory: Path) -> List[str]:
    out = subprocess.check_output(create_find_command(directory))
    return out.decode("utf-8").split("\n")


def get_checksums_dict(line_list) -> Dict[Path, str]:
    files = []
    checksums = []
    for line in line_list:
        if len(line) == 0:
            continue
        checksum, file_name = line.split("  ")  # Coupled to output of sha256sum.
        checksums.append(checksum.strip())
        files.append(Path(file_name.strip()))
    common_file_prefix = os.path.commonprefix(files)
    return dict(
        zip([Path(str(file)[len(common_file_prefix) :]) for file in files], checksums)
    )


def get_filtered_checksums_dict(line_list) -> Dict[Path, str]:
    checksums_dict = get_checksums_dict(line_list)
    for config_file in config_files.CONFIG_FILES:
        if config_file in checksums_dict:
            del checksums_dict[Path(config_file)]
    return checksums_dict


def calculate_files_to_be_removed(
    local_checksums: Dict[Path, str], remote_checksums: Dict[Path, str]
) -> List[Path]:
    return [
        file
        for file in remote_checksums.keys()
        if file not in local_checksums.keys()
        and not str(file).startswith(
            TARGETS["staging"]
        )  # Do not delete staging while updating production.
    ]


def calculate_files_to_be_uploaded(
    local_checksums: Dict[Path, str], remote_checksums: Dict[Path, str]
) -> List[Path]:
    return [
        file
        for file in local_checksums.keys()
        if file not in remote_checksums.keys()
        or local_checksums[file] != remote_checksums[file]
    ]


def remove_on_remote(ssh_client, files: List[Path]):
    ssh_client.exec_command("rm " + " ".join([str(file) for file in files]))


def remove_outdated_files(
    ssh_client,
    local_checksums: Dict[Path, str],
    remote_checksums: Dict[Path, str],
):
    files_to_be_removed = calculate_files_to_be_removed(
        local_checksums=local_checksums,
        remote_checksums=remote_checksums,
    )
    remove_on_remote(
        ssh_client=ssh_client,
        files=files_to_be_removed,
    )
    print(f"Removed files: {files_to_be_removed}")


def upload_updated_files(
    ssh_client,
    remote_path: str,
    local_path: Path,
    local_checksums: Dict[Path, str],
    remote_checksums: Dict[Path, str],
):
    files_to_be_uploaded = calculate_files_to_be_uploaded(
        local_checksums=local_checksums,
        remote_checksums=remote_checksums,
    )
    file_transfer.upload_files(
        ssh_client=ssh_client,
        file_paths=[str(file) for file in files_to_be_uploaded],
        target_root_path=remote_path,
        local_path_prefix=str(local_path),
    )


def create_version_information():
    repo = git.Repo(GIT_REPO_PATH)
    version_info = (
        f"Deploy Date: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}\n"
        f"Commit Hash: {repo.head.commit.hexsha}\n"
        f"Commit Date: {datetime.fromtimestamp(repo.head.commit.committed_date)}\n"
    )
    git_diff = [item.a_path for item in repo.index.diff(None)]
    if len(git_diff) > 0:
        version_info += "Dirty files:\n    " + "\n    ".join(
            [item.a_path for item in repo.index.diff(None)]
        )
    return version_info


def store_version_information(ssh_client, root_path):
    version_info = create_version_information()
    print(
        f"Store new version.txt file with content:\n"
        f"{textwrap.indent(version_info, '    ')}"
    )
    ssh_client.exec_command(f'echo "{version_info}" > {root_path}/version.txt')


def validate_arguments(target: str):
    if target == "production":
        repo = git.Repo(GIT_REPO_PATH)
        git_diff = [item.a_path for item in repo.index.diff(None)]
        if len(git_diff) != 0:
            print("Cannot deploy to production in dirty state.")
            exit(1)


def main():
    args = get_args()
    validate_arguments(args.target)

    ssh_client = ssh.connect(host=ssh.DEFAULT_HOST, user=ssh.DEFAULT_USER)

    local_path = GIT_REPO_PATH / "content"
    local_checksum_line_list = create_local_checksum_line_list(local_path)

    remote_path = TARGETS[args.target]
    remote_checksum_line_list = create_remote_checksum_line_list(
        ssh_client=ssh_client, directory=remote_path
    )
    if args.config:
        remote_checksums = get_checksums_dict(remote_checksum_line_list)
        local_checksums = get_checksums_dict(local_checksum_line_list)
    else:
        remote_checksums = get_filtered_checksums_dict(remote_checksum_line_list)
        local_checksums = get_filtered_checksums_dict(local_checksum_line_list)

    remove_outdated_files(
        ssh_client=ssh_client,
        local_checksums=local_checksums,
        remote_checksums=remote_checksums,
    )
    upload_updated_files(
        ssh_client=ssh_client,
        remote_path=remote_path,
        local_path=local_path,
        local_checksums=local_checksums,
        remote_checksums=remote_checksums,
    )
    store_version_information(
        ssh_client=ssh_client,
        root_path=remote_path,
    )


if __name__ == "__main__":
    main()
