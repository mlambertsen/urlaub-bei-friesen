import os
import contextlib
import subprocess

from pathlib import Path
from typing import Iterator

from urlaub_bei_friesen.common.path import GIT_REPO_PATH


@contextlib.contextmanager
def _working_directory(path: Path) -> Iterator[None]:
    # Taken from https://stackoverflow.com/questions/41742317/how-can-i-change-directory-with-python-pathlib # noqa
    """Changes working directory and returns to previous on exit."""
    prev_cwd = Path.cwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(prev_cwd)


def _run_tests() -> None:
    print("Running unit tests:")
    with _working_directory(GIT_REPO_PATH / "python"):
        subprocess.call(
            [
                "py.test",
                "--color=yes",
                "--cov=urlaub_bei_friesen",
                "--cov-report=term-missing",
                "--no-cov-on-fail",
                "--import-mode=importlib",
            ]
        )


def _run_mypy() -> None:
    print("Running mypy:")
    with _working_directory(GIT_REPO_PATH / "python"):
        subprocess.call(
            [
                "mypy",
                "-p",
                "urlaub_bei_friesen",
            ]
        )


def _run_flake() -> None:
    print("Running flake8:")
    with _working_directory(GIT_REPO_PATH / "python"):
        subprocess.call(
            [
                "flake8",
                ".",
            ]
        )


def main():
    _run_tests()
    _run_mypy()
    _run_flake()


if __name__ == "__main__":
    main()
