import PySimpleGUI as sg

from typing import Optional, Tuple

from urlaub_bei_friesen.common.icon import ICON_BASE_64

from .element_factory import create_button


CANCEL = "Abbrechen"
OK = "Ok"
FROM_KEY = "RANGE_SELECTION_FROM_KEY"
TO_KEY = "RANGE_SELECTION_TO_KEY"


def _create_range_selection_layout(
    introduction_sentence: str,
    conjunction: str,
    postfix: Optional[str] = None,
    default_from_value: Optional[str] = None,
    default_to_value: Optional[str] = None,
):
    return [
        [sg.Text(introduction_sentence)],
        [
            sg.InputText(
                key=FROM_KEY,
                focus=True,
                size=(5, 1),
                default_text=default_from_value
                if default_from_value is not None
                else "",
            ),
            sg.Text(conjunction),
            sg.InputText(
                key=TO_KEY,
                size=(5, 1),
                default_text=default_to_value if default_to_value is not None else "",
            ),
            sg.Text(postfix) if postfix is not None else sg.Text(),
        ],
        [
            create_button(
                button_text=OK,
                bind_return_key=True,
            ),
            create_button(
                button_text=CANCEL,
            ),
        ],
    ]


def select_range(
    title_postfix: str,
    introduction_sentence: str,
    range_conjunction: str,
    range_postfix: Optional[str] = None,
    default_from_value: Optional[str] = None,
    default_to_value: Optional[str] = None,
) -> Optional[Tuple[str, str]]:
    window = sg.Window(
        title="Urlaub bei Friesen - " + title_postfix,
        layout=_create_range_selection_layout(
            introduction_sentence,
            range_conjunction,
            range_postfix,
            default_from_value,
            default_to_value,
        ),
        icon=ICON_BASE_64,
    )

    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED or event == CANCEL:
            window.close()
            return None
        if event == OK:
            from_str = window[FROM_KEY].get()
            to_str = window[TO_KEY].get()
            window.close()
            return from_str, to_str
