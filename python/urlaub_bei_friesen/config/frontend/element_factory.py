import PySimpleGUI as sg

import urlaub_bei_friesen.common.colors as colors


def create_button(
    button_text: str,
    key: str = None,
    focus: bool = False,
    bind_return_key: bool = False,
    visible: bool = True,
):
    if not key:
        key = button_text
    return sg.Button(
        button_text=button_text,
        key=key,
        highlight_colors=(colors.MAIN_COLOR_DARK, colors.MAIN_COLOR_LIGHT),
        focus=focus,
        bind_return_key=bind_return_key,
        visible=visible,
    )
