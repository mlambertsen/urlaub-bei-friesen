import PySimpleGUI as sg

from typing import Optional

from urlaub_bei_friesen.common.icon import ICON_BASE_64

from ..backend.update_ui import UpdateUi
from .element_factory import create_button


class UpdateUiPySimpleGui(UpdateUi):
    _TEXT_KEY: str = "update_text"
    _BUTTON_KEY: str = "update_ok"
    _WIDTH: int = 50

    _window: Optional[sg.Window]
    _messages: str

    def __init__(self):
        self._window = None
        self._message = ""

    def request_approval_to_update(self, message: str) -> bool:
        positive_answer = "Ja"
        negative_answer = "Nein"
        user_decision = sg.popup(
            message,
            title="Updates verfügbar",
            custom_text=(positive_answer, negative_answer),
            icon=ICON_BASE_64,
        )
        return user_decision == positive_answer

    def print_info(self, message: str) -> None:
        self.initialize_window()
        assert self._window is not None
        if len(self._message) > 0:
            self._message += " Fertig!\n"
        self._message += message
        self._window.read(timeout=0)
        self._window[UpdateUiPySimpleGui._TEXT_KEY].set_size(
            (UpdateUiPySimpleGui._WIDTH, len(self._message.splitlines()))
        )
        self._window[UpdateUiPySimpleGui._TEXT_KEY].update(self._message)
        self._window.refresh()

    def print_final_info(self, message: str) -> None:
        self.print_info(message)
        assert self._window is not None
        self._window[UpdateUiPySimpleGui._BUTTON_KEY].update(visible=True)
        while True:
            event, values = self._window.read()
            if event == sg.WIN_CLOSED or event == UpdateUiPySimpleGui._BUTTON_KEY:
                self._window.close()
                return

    def initialize_window(self):
        if self._window is None:
            self._window = sg.Window(
                title="Update Urlaub bei Friesen",
                layout=[
                    [
                        sg.Text(
                            key=UpdateUiPySimpleGui._TEXT_KEY,
                            size=(UpdateUiPySimpleGui._WIDTH, 1),
                        ),
                    ],
                    [
                        create_button(
                            button_text="Ok",
                            key=UpdateUiPySimpleGui._BUTTON_KEY,
                            focus=False,
                            bind_return_key=False,
                            visible=False,
                        )
                    ],
                ],
                icon=ICON_BASE_64,
            )
