from datetime import datetime
import PySimpleGUI as sg

from typing import List, Optional

from urlaub_bei_friesen.common.icon import ICON_BASE_64

from ..backend.date_utils import DateRange
from .element_factory import create_button


CANCEL = "Abbrechen"
OK = "Ok"
FROM_KEY = "Von"
TO_KEY = "Bis"


def _create_single_date_selection_layout(label: str) -> List[List[sg.Element]]:
    return [
        [
            sg.CalendarButton(
                button_text=label,
                target=label,
                format="%d.%m.%Y",
                key=label + "button",
                default_date_m_d_y=(
                    datetime.today().month,
                    datetime.today().day,
                    datetime.today().year,
                ),
                begin_at_sunday_plus=1,
                day_abbreviations=["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
                month_names=[
                    "Januar",
                    "Februar",
                    "März",
                    "April",
                    "Mai",
                    "Juni",
                    "Juli",
                    "August",
                    "September",
                    "Oktober",
                    "November",
                    "Dezember",
                ],
            )
        ],
        [sg.Text("", key=label)],
    ]


def _create_date_selection_layout() -> List[List[sg.Element]]:
    return [
        [
            sg.Column(_create_single_date_selection_layout(FROM_KEY)),
            sg.Column(_create_single_date_selection_layout(TO_KEY)),
        ],
        [
            create_button(
                button_text=OK,
                bind_return_key=True,
            ),
            create_button(
                button_text=CANCEL,
            ),
        ],
    ]


def select_date_range() -> Optional[DateRange]:
    window = sg.Window(
        title="Urlaub bei Friesen - Wähle Zeitraum",
        layout=_create_date_selection_layout(),
        element_justification="c",
        icon=ICON_BASE_64,
    )

    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED or event == CANCEL:
            window.close()
            return None
        if event == OK:
            from_date = window[FROM_KEY].get()
            to_date = window[TO_KEY].get()
            if from_date and to_date:
                window.close()
                return DateRange.create_from_strings(from_date, to_date)
            else:
                sg.popup_ok(
                    "Es müssen beide Daten ausgewählt werden.",
                    title="Eingabefehler",
                    icon=ICON_BASE_64,
                )
