import PySimpleGUI as sg

import urlaub_bei_friesen.common.colors as colors
from urlaub_bei_friesen.common.icon import ICON_BASE_64
from urlaub_bei_friesen.common.ssh import DEFAULT_HOST, DEFAULT_USER

from ..backend.ssh_ui import SshCredentials, SshUi
from .element_factory import create_button


class SshUiPySimpleGui(SshUi):
    def request_user_credentials(self, error_text: str = "") -> SshCredentials:
        is_error_text_visible = len(error_text) != 0
        layout = [
            [
                sg.Text(
                    text=error_text,
                    visible=is_error_text_visible,
                )
            ],
            [
                sg.Text(text="Host:", size=(10, 1)),
                sg.InputText(
                    default_text=DEFAULT_HOST,
                    key="Host",
                    readonly=True,
                    text_color=colors.TEXT_COLOR,
                ),
            ],
            [
                sg.Text(text="User:", size=(10, 1)),
                sg.InputText(
                    default_text=DEFAULT_USER,
                    key="User",
                    readonly=True,
                    text_color=colors.TEXT_COLOR,
                ),
            ],
            [
                sg.Text(text="Password:", size=(10, 1)),
                sg.InputText(password_char="*", key="Password", focus=True),
            ],
            [create_button("Login", bind_return_key=True)],
        ]
        window = sg.Window(
            title="Urlaub bei Friesen - Login",
            layout=layout,
            icon=ICON_BASE_64,
        )

        while True:
            event, values = window.read()
            if event == sg.WIN_CLOSED:
                exit(0)
            if event == "Login":
                window.close()
                return SshCredentials(
                    host=values["Host"],
                    user=values["User"],
                    password=values["Password"],
                )
