import PySimpleGUI as sg
from typing import List, Optional

from urlaub_bei_friesen.common.icon import ICON_BASE_64

from ..backend.main_ui import MainUi
from ..backend.config_ui import ConfigUi
from .element_factory import create_button

CANCEL_AND_EXIT = "Abbrechen & Beenden"
FINISH_AND_UPLOAD = "Fertig & Hochladen"


def create_layout(configs: List[ConfigUi]) -> List[List[sg.Element]]:
    config_tabs = [config.get_layout() for config in configs]
    layout = [
        [sg.TabGroup(layout=[config_tabs])],
        [
            create_button(FINISH_AND_UPLOAD),
            create_button(CANCEL_AND_EXIT),
        ],
    ]
    return layout


class MainUiSimpleGui(MainUi):
    configs: List[ConfigUi]
    window: Optional[sg.Window]

    def __init__(self):
        self.configs = []

    def add_config(self, config_ui: ConfigUi) -> None:
        self.configs.append(config_ui)
        self.window = None

    def start(self) -> None:
        self.window = sg.Window(
            title="Urlaub bei Friesen",
            layout=create_layout(configs=self.configs),
            element_justification="r",
            icon=ICON_BASE_64,
        )
        while True:
            event, values = self.window.read()
            if event == sg.WIN_CLOSED or event == CANCEL_AND_EXIT:
                self.window.close()
                return
            elif event == FINISH_AND_UPLOAD:
                for config in self.configs:
                    config.upload()
                self.window.close()
                return
            else:
                for config in self.configs:
                    config.update(event=event, callback=self._callback_access_window)

    def _callback_access_window(self):
        return self.window
