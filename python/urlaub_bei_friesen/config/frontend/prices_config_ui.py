import PySimpleGUI as sg

from functools import partial
from typing import Callable, List, Optional, Tuple

from urlaub_bei_friesen.common.icon import ICON_BASE_64

from ..backend.config_ui import ConfigUi
from ..backend.date_utils import create_date_format, DateRange
from ..backend.prices import Prices
from .range_selection import select_range
from .date_range_selection import select_date_range

_PRICE_KEY_PREFIX = "PRICE_KEY_"
_PRICE_KEY_SEPARATOR = "#"
_PRICE_MAIN_SEASON_KEY_PREFIX = "PRICE_MAIN_SEASON_"


def _create_validity_key() -> str:
    return _PRICE_KEY_PREFIX + "VALIDITY"


def _create_main_season_key(season_index: int) -> str:
    return _PRICE_KEY_SEPARATOR.join([_PRICE_MAIN_SEASON_KEY_PREFIX, str(season_index)])


def _create_price_key(season: str, number_persons: str) -> str:
    return _PRICE_KEY_SEPARATOR.join([_PRICE_KEY_PREFIX, season, number_persons])


def _noop() -> None:
    ...


def _determine_action_from_key(
    event: str, prices: Prices, callback: Callable
) -> Callable:
    if isinstance(event, str):
        if event == _create_validity_key():
            return partial(_update_price_validity, prices, callback)
        if event.startswith(_PRICE_KEY_PREFIX):
            _, season, number_persons = event.split(_PRICE_KEY_SEPARATOR)
            return partial(
                _update_price_range,
                season,
                number_persons,
                prices,
                callback,
            )
        if event.startswith(_PRICE_MAIN_SEASON_KEY_PREFIX):
            _, main_season_index = event.split(_PRICE_KEY_SEPARATOR)
            return partial(
                _update_main_season,
                int(main_season_index),
                prices,
                callback,
            )
    return _noop


def _create_prices_column(prices: Prices, number_persons: str) -> sg.Column:
    column_entries: List[List[sg.Element]] = [
        [
            sg.Button(
                button_text=f"{prices.get_price_table_entry(season, number_persons)}€",
                key=_create_price_key(season, number_persons),
                pad=(0, 0),
            )
        ]
        for season in prices.seasons
    ]
    column_entries.insert(0, [sg.Text(number_persons)])
    return sg.Column(column_entries, element_justification="c")


def _create_seasons_column(prices: Prices) -> sg.Column:
    season_column = [[sg.Text(season)] for season in prices.seasons]
    season_column.insert(0, [sg.Text()])
    return sg.Column(season_column, element_justification="l")


def _create_price_table_layout(prices: Prices) -> List[sg.Column]:
    columns: List[sg.Column] = [
        _create_prices_column(prices, number_persons)
        for number_persons in prices.number_persons
    ]
    columns.insert(0, _create_seasons_column(prices))
    return columns


def _create_main_seasons_layout(main_seasons: List[DateRange]) -> List[sg.Column]:
    return [
        sg.Column([[sg.Text("Zeiträume Hauptsaison: ")]]),
        sg.Column(
            [
                [
                    sg.Button(
                        button_text=main_season.to_string(
                            conjunction="bis",
                            date_format=create_date_format(
                                month_as_text=True, include_year=False
                            ),
                        ),
                        key=_create_main_season_key(index),
                    )
                ]
                for index, main_season in enumerate(main_seasons)
            ]
        ),
    ]


def _create_validity_layout(validity: DateRange) -> List[sg.Element]:
    return [
        sg.Text("Gültigkeit der Preise: "),
        sg.Button(button_text=f"{str(validity)}", key=_create_validity_key()),
    ]


def _select_price_range(
    season: str, number_persons: str, prices: Prices
) -> Optional[Tuple[str, str]]:
    return select_range(
        title_postfix="Preis auswählen",
        introduction_sentence=f"Wähle die Preisspanne für eine Wohnung "
        f"in der {season} für {number_persons}:",
        range_conjunction="-",
        range_postfix="€",
        default_from_value=prices.get_price_table_entry(season, number_persons).split(
            " "
        )[0],
        default_to_value=prices.get_price_table_entry(season, number_persons).split(
            " "
        )[2],
    )


def _update_price_range(
    season: str, number_persons: str, prices: Prices, callback: Callable
) -> None:
    price_range = _select_price_range(season, number_persons, prices)
    if price_range is not None:
        from_price, to_price = price_range
        prices.set_price_range(season, number_persons, from_price, to_price)
        window: sg.Window = callback()
        window[_create_price_key(season, number_persons)].update(
            text=prices.get_price_table_entry(season, number_persons) + "€"
        )


def _update_main_season(
    main_season_index: int, prices: Prices, callback: Callable
) -> None:
    new_main_season = select_date_range()
    if new_main_season is not None:
        main_seasons: List[DateRange] = prices.main_seasons
        main_seasons[main_season_index] = new_main_season
        window: sg.Window = callback()
        window[_create_main_season_key(main_season_index)].update(
            text=new_main_season.to_string(
                conjunction="bis",
                date_format=create_date_format(month_as_text=True, include_year=False),
            )
        )


def _update_price_validity(prices: Prices, callback: Callable) -> None:
    new_validity = select_date_range()
    if new_validity is not None:
        prices.validity = new_validity
        window: sg.Window = callback()
        window[_create_validity_key()].update(text=f"{prices.validity}")


class PricesConfigUiSimpleGui(ConfigUi):
    _prices: Prices

    def __init__(self, prices: Prices):
        self._prices = prices

    def update(self, event: str, callback: Callable) -> None:
        action = _determine_action_from_key(event, self._prices, callback)
        try:
            action()
        except ValueError as error:
            sg.popup_ok(
                f"Eingabe konnte nicht benutzt werden.\n"
                f"Aufgetretener Fehler: {error}",
                title="Eingabefehler",
                icon=ICON_BASE_64,
            )

    def upload(self) -> None:
        self._prices.upload()

    def get_layout(self) -> sg.Tab:
        return sg.Tab(
            "Preise",
            [
                _create_price_table_layout(self._prices),
                [sg.HorizontalSeparator()],
                _create_validity_layout(self._prices.validity),
                [sg.HorizontalSeparator()],
                _create_main_seasons_layout(self._prices.main_seasons),
            ],
        )
