import PySimpleGUI as sg

from typing import Callable, List, Tuple

from urlaub_bei_friesen.common.icon import ICON_BASE_64

from ..backend.config_ui import ConfigUi
from ..backend.date_utils import DateList, DateRange, date_list_to_ranges_of_dates
from ..backend.occupancy import Occupancy
from .date_range_selection import select_date_range
from .element_factory import create_button


def _update_occupancy_entries(
    window: sg.Window, apartment_name: str, occupancies: List[DateRange]
) -> None:
    key = _create_key(
        prefix=OCCUPANCY_LISTBOX_KEY_PREFIX, apartment_name=apartment_name
    )
    window[key].set_size((None, len(occupancies)))
    window[key].Update(values=[str(occupancy) for occupancy in occupancies])


def _add_time_range(apartment_dates: DateList, range_to_add: DateRange) -> None:
    apartment_dates.add_date_range(range_to_add)


def _remove_time_range(apartment_dates: DateList, range_to_remove: DateRange) -> None:
    apartment_dates.remove_date_range(range_to_remove)


OCCUPANCY_LISTBOX_KEY_PREFIX = "occupancy_entries"

ACTIONS = {
    "Zeitraum hinzufügen": _add_time_range,
    "Zeitraum entfernen": _remove_time_range,
}


def _create_key(prefix: str, apartment_name: str) -> str:
    return prefix + apartment_name


def _demangle_key(event: str) -> Tuple[str, str]:
    for action in ACTIONS.keys():
        if isinstance(event, str) and event.startswith(action):
            return event[: len(action)], event[len(action) :]
    raise KeyError("Event no action for OccupancyConfigUi.")


def _create_single_apartment_occupancy_column_layout(
    apartment_name: str, apartment_occupancies: List[DateRange]
) -> List[List[sg.Element]]:
    return [
        [sg.Text(text="Wohnung: " + apartment_name)],
        [
            create_button(
                button_text=action,
                key=_create_key(prefix=action, apartment_name=apartment_name),
            )
            for action in ACTIONS.keys()
        ],
        [
            sg.Listbox(
                values=[str(occupancy) for occupancy in apartment_occupancies],
                enable_events=False,
                size=(30, len(apartment_occupancies)),
                no_scrollbar=True,
                key=_create_key(
                    prefix=OCCUPANCY_LISTBOX_KEY_PREFIX, apartment_name=apartment_name
                ),
            )
        ],
    ]


class OccupancyConfigUiSimpleGui(ConfigUi):
    occupancy: Occupancy
    keys_to_react: List[str]

    def __init__(self, occupancy: Occupancy):
        self.occupancy = occupancy
        self.keys_to_react = []
        for prefix in ACTIONS.keys():
            self.keys_to_react.extend(
                [
                    _create_key(prefix, apartment_name)
                    for apartment_name in self.occupancy.apartments.keys()
                ]
            )

    def update(self, event: str, callback: Callable) -> None:
        try:
            action, apartment_name = _demangle_key(event)
        except KeyError:
            return

        try:
            date_range = select_date_range()
            if date_range is None:
                return
            updated_apartment_dates = self.occupancy.apartments[apartment_name][
                "date_list"
            ]
            ACTIONS[action](updated_apartment_dates, date_range)
            _update_occupancy_entries(
                window=callback(),
                apartment_name=apartment_name,
                occupancies=date_list_to_ranges_of_dates(updated_apartment_dates),
            )
        except ValueError as ex:
            sg.popup_ok(
                "Daten konnten nicht übernommen werden:\n" + str(ex),
                title="Eingabefehler",
                icon=ICON_BASE_64,
            )
            return

    def upload(self) -> None:
        self.occupancy.upload()

    def get_layout(self) -> sg.Tab:
        return sg.Tab(
            "Belegung",
            [
                [
                    sg.Column(
                        layout=_create_single_apartment_occupancy_column_layout(
                            apartment_name,
                            date_list_to_ranges_of_dates(
                                apartment_occupancies["date_list"]
                            ),
                        ),
                        vertical_alignment="top",
                    )
                    for apartment_name, apartment_occupancies in self.occupancy.apartments.items()  # noqa
                ]
            ],
        )
