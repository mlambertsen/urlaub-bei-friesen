from abc import ABC, abstractmethod
from typing import Any, Callable


class ConfigUi(ABC):
    @abstractmethod
    def get_layout(self) -> Any:
        ...

    @abstractmethod
    def update(self, event: str, callback: Callable) -> None:
        ...

    @abstractmethod
    def upload(self) -> None:
        ...
