from abc import ABC, abstractmethod


class SshCredentials:
    def __init__(self, host: str, user: str, password: str):
        self.host = host
        self.user = user
        self.password = password


class SshUi(ABC):
    @abstractmethod
    def request_user_credentials(self, error_text: str = "") -> SshCredentials:
        ...
