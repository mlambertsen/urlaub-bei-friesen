from paramiko import SSHClient
from pathlib import Path
from typing import Any, Dict, List

from urlaub_bei_friesen.common.config_files import (
    OCCUPANCY_LEFT_FILE_PATH,
    OCCUPANCY_RIGHT_FILE_PATH,
)
from urlaub_bei_friesen.common.file_transfer import download_files, upload_files
from urlaub_bei_friesen.common.path import GIT_REPO_PATH
from .date_utils import (
    DateList,
    date_list_to_ranges_of_dates,
    load_date_ranges_from_file,
)


def _download_occupancy_files(ssh: SSHClient):
    target_dir = GIT_REPO_PATH / "content"
    download_files(
        ssh_client=ssh,
        src_paths=[
            OCCUPANCY_LEFT_FILE_PATH,
            OCCUPANCY_RIGHT_FILE_PATH,
        ],
        target_root_path=str(target_dir),
    )
    return target_dir / OCCUPANCY_LEFT_FILE_PATH, target_dir / OCCUPANCY_RIGHT_FILE_PATH


def _upload_files(ssh: SSHClient, occupancy_files: List[str]) -> None:
    local_dir = GIT_REPO_PATH / "content"
    upload_files(
        ssh_client=ssh,
        file_paths=[str(file)[len(str(local_dir)) + 1 :] for file in occupancy_files],
        target_root_path=".",
        local_path_prefix=str(local_dir),
    )


def _write_occupancy_file(file_path: Path, data: DateList):
    with open(file_path, "w") as file_handle:
        date_ranges = date_list_to_ranges_of_dates(data)
        file_handle.writelines([str(date_range) + "\n" for date_range in date_ranges])


def _load_date_list_from_file(file_path: Path) -> DateList:
    return DateList(ranges=load_date_ranges_from_file(file_path))


class Occupancy:
    ssh: SSHClient
    apartments: Dict[str, Dict[str, Any]]

    def __init__(self, ssh: SSHClient):
        self.ssh = ssh
        left, right = _download_occupancy_files(self.ssh)
        self.apartments = {
            "Links": {"path": left, "date_list": _load_date_list_from_file(left)},
            "Rechts": {"path": right, "date_list": _load_date_list_from_file(right)},
        }

    def upload(self):
        files_to_upload = []
        for apartment in self.apartments.values():
            files_to_upload.append(apartment["path"])
            _write_occupancy_file(
                file_path=apartment["path"], data=apartment["date_list"]
            )
        _upload_files(self.ssh, files_to_upload)
