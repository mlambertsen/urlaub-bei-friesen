from abc import ABC, abstractmethod
from .config_ui import ConfigUi


class MainUi(ABC):
    @abstractmethod
    def add_config(self, config_ui: ConfigUi) -> None:
        ...

    @abstractmethod
    def start(self) -> None:
        ...
