import json

from paramiko import SSHClient
from pathlib import Path
from typing import Any, Dict, List, Tuple

from urlaub_bei_friesen.common.config_files import (
    PRICES_FILE_PATH,
    PRICES_MAIN_SEASON_FILE_PATH,
    PRICES_VALIDITY_FILE_PATH,
)
from urlaub_bei_friesen.common.file_transfer import download_files, upload_files
from urlaub_bei_friesen.common.path import GIT_REPO_PATH

from .date_utils import create_date_format, DateRange, load_date_ranges_from_file


def _download_prices_files(ssh: SSHClient) -> Tuple[Path, Path, Path]:
    target_dir = GIT_REPO_PATH / "content"
    download_files(
        ssh_client=ssh,
        src_paths=[
            PRICES_FILE_PATH,
            PRICES_MAIN_SEASON_FILE_PATH,
            PRICES_VALIDITY_FILE_PATH,
        ],
        target_root_path=str(target_dir),
    )
    return (
        target_dir / PRICES_FILE_PATH,
        target_dir / PRICES_MAIN_SEASON_FILE_PATH,
        target_dir / PRICES_VALIDITY_FILE_PATH,
    )


def _upload_files(ssh: SSHClient, files: List[str]) -> None:
    local_dir = GIT_REPO_PATH / "content"
    upload_files(
        ssh_client=ssh,
        file_paths=[str(file)[len(str(local_dir)) + 1 :] for file in files],
        target_root_path=".",
        local_path_prefix=str(local_dir),
    )


def _write_prices_file(file_path: Path, json_string: str) -> None:
    with file_path.open(mode="w") as file_handle:
        json.dump(json_string, file_handle, indent=2)


def _read_prices_file(prices_file: Path) -> str:
    with prices_file.open(mode="r") as file_handle:
        return json.load(file_handle)


def _write_main_season_file(
    main_season_file: Path, main_season_date_ranges: List[DateRange]
) -> None:
    with main_season_file.open(mode="w") as file_handle:
        file_handle.write(
            "\n".join(
                [
                    date_range.to_string(
                        conjunction="bis",
                        date_format=create_date_format(
                            month_as_text=True, include_year=False
                        ),
                    )
                    for date_range in main_season_date_ranges
                ]
            )
        )


def _read_main_season_file(main_season_file: Path) -> List[DateRange]:
    with main_season_file.open(mode="r") as file_handle:
        return [
            DateRange.create_from_string(line, conjunction="bis")
            for line in file_handle.readlines()
        ]


def _write_validity_file(validity_file: Path, date_range: DateRange) -> None:
    with validity_file.open(mode="w") as file_handle:
        file_handle.write(str(date_range))


def _read_validity_file(validity_file: Path) -> DateRange:
    data_ranges = load_date_ranges_from_file(validity_file)
    if len(data_ranges) != 1:
        raise RuntimeError("Validity files has unexpected format.")
    return data_ranges[0]


class Prices:
    _ssh: SSHClient
    _prices: Dict[str, Dict[str, Any]]

    def __init__(self, ssh: SSHClient):
        self._ssh = ssh
        prices_file, main_season_file, validity_file = _download_prices_files(self._ssh)
        self._prices = {
            "prices": {
                "path": prices_file,
                "data": _read_prices_file(prices_file),
            },
            "validity": {
                "path": validity_file,
                "data": _read_validity_file(validity_file),
            },
            "main_season": {
                "path": main_season_file,
                "data": _read_main_season_file(main_season_file),
            },
        }

    @property
    def seasons(self) -> List[str]:
        return self._prices["prices"]["data"].keys()

    @property
    def number_persons(self) -> List[str]:
        return next(iter(self._prices["prices"]["data"].values())).keys()

    @property
    def validity(self) -> DateRange:
        return self._prices["validity"]["data"]

    @validity.setter
    def validity(self, new_value: DateRange) -> None:
        self._prices["validity"]["data"] = new_value

    @property
    def main_seasons(self) -> List[DateRange]:
        return self._prices["main_season"]["data"]

    @main_seasons.setter
    def main_seasons(self, new_value: List[DateRange]) -> None:
        self._prices["main_season"]["data"] = new_value

    def get_price_table_entry(self, season: str, number_persons: str) -> str:
        return self._prices["prices"]["data"][season][number_persons]

    def set_price_range(
        self, season: str, number_persons: str, from_price: str, to_price: str
    ):
        if not (from_price.isnumeric() and to_price.isnumeric()):
            raise ValueError("Preisspanne besteht nicht aus positiven Zahlen.")
        if int(from_price) > int(to_price):
            raise ValueError("Erster Preis ist größer als zweiter Preis.")
        self._prices["prices"]["data"][season][number_persons] = " - ".join(
            [from_price, to_price]
        )

    def upload(self):
        _write_prices_file(
            self._prices["prices"]["path"], self._prices["prices"]["data"]
        )
        _write_main_season_file(
            self._prices["main_season"]["path"], self._prices["main_season"]["data"]
        )
        _write_validity_file(self._prices["validity"]["path"], self.validity)
        _upload_files(self._ssh, [elem["path"] for elem in self._prices.values()])
