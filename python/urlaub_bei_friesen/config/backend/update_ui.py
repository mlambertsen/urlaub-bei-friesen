from abc import ABC, abstractmethod


class UpdateUi(ABC):
    @abstractmethod
    def request_approval_to_update(self, message: str) -> bool:
        ...

    @abstractmethod
    def print_info(self, message: str) -> None:
        ...

    @abstractmethod
    def print_final_info(self, message: str) -> None:
        ...
