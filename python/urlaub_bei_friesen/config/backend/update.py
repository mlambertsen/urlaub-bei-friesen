import git
import os
import subprocess

from urlaub_bei_friesen.common.path import GIT_REPO_PATH
from .update_ui import UpdateUi


def is_branch_considered_for_updates(repo: git.Repo):
    branch = repo.active_branch
    return branch.name in ["master", "develop"]


def get_origin_commits(repo: git.Repo):
    repo.remotes.origin.fetch()
    return {
        "master": repo.remotes.origin.refs.master.commit,
        "develop": repo.remotes.origin.refs.develop.commit,
    }


def get_all_files_under_source_control(repo: git.Repo):
    files_under_source_control = repo.git.execute(
        [
            "git",
            "ls-tree",
            "-r",
            "--name-only",
            repo.active_branch.commit.hexsha,
            str(GIT_REPO_PATH),
        ]
    ).split()
    return [os.path.join(GIT_REPO_PATH, file) for file in files_under_source_control]


def get_titles_of_new_commits_on_origin(repo: git.Repo):
    head = repo.head.ref
    tracking = head.tracking_branch()
    return [
        f.message.split("\n")[0]
        for f in tracking.commit.iter_items(repo, f"{head.path}..{tracking.path}")
    ]


def perform_update(repo: git.Repo, ui: UpdateUi) -> None:
    ui.print_info("Setze lokale Änderungen zurück...")
    git.IndexFile(repo=repo).checkout(
        paths=get_all_files_under_source_control(repo),
        force=True,
    )
    ui.print_info("Lese Liste von Updates...")
    new_commits = get_titles_of_new_commits_on_origin(repo)
    ui.print_info("Wende Updates an...")
    repo.remotes.origin.pull()
    ui.print_info("Kompiliere requirements.txt...")
    subprocess.check_call(["bash", f"{GIT_REPO_PATH}/python/compile_requirements.sh"])
    ui.print_info("Erstelle virtuelle Umgebung...")
    subprocess.check_call(["bash", f"{GIT_REPO_PATH}/python/setup_venv.sh"])
    new_commits.insert(0, "Folgende Updates wurden erfolgreich geladen:")
    ui.print_final_info(
        "\n" + "\n * ".join(new_commits) + "\n\nBitte Programm neu starten."
    )
    exit(0)


def is_user_accepting_update(ui: UpdateUi):
    return ui.request_approval_to_update(
        message="Sollen neue Updates installiert werden?\n"
        "Dies setzt alle lokalen Änderungen bis auf"
        " Konfigurationsdateien an der Homepage zurück.",
    )


def check_for_updates(ui: UpdateUi):
    repo = git.Repo(GIT_REPO_PATH)
    if is_branch_considered_for_updates(repo):
        origin_commits = get_origin_commits(repo)
        if repo.head.commit != origin_commits[repo.active_branch.name]:
            if is_user_accepting_update(ui):
                perform_update(repo, ui)
