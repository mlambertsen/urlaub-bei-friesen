import datetime

from babel.dates import format_date, parse_date
from pathlib import Path
from typing import Iterable, List, Tuple


_LOCALE = "de_DE"


def _get_german_months():
    return [
        "Januar",
        "Februar",
        "März",
        "April",
        "Mai",
        "Juni",
        "Juli",
        "August",
        "September",
        "Oktober",
        "November",
        "Dezember",
    ]


def _get_german_month_to_month_index():
    return {month: index for index, month in enumerate(_get_german_months(), start=1)}


def convert_string_to_date(
    string: str, default_year: int = datetime.date.today().year
) -> datetime.date:
    try:
        return parse_date(string=string, locale=_LOCALE)
    except IndexError:
        # Babel can as of now not parse dates with month names...
        date = string.replace(".", " ").replace("/", " ").strip().split()
        if len(date) == 2:
            day, month = date
            year = str(default_year)
        elif len(date) == 3:
            day, month, year = date
        else:
            raise ValueError(f"Datum in unbekanntem Format: '{string}'")

        fallback_month = (
            month
            if month.isnumeric()
            else str(_get_german_month_to_month_index()[month])
        )
        fallback_string = ".".join([day, fallback_month, year])
        return parse_date(string=fallback_string, locale=_LOCALE)


def create_date_format(month_as_text: bool = False, include_year: bool = True) -> str:
    date_format = "dd"
    date_format += ". LLLL" if month_as_text else ".MM"
    date_format += "." if not month_as_text and include_year else ""
    date_format += " " if month_as_text and include_year else ""
    date_format += "YYYY" if include_year else ""
    return date_format


class DateRange:
    DEFAULT_CONJUNCTION: str = "-"
    _start: datetime.date
    _end: datetime.date

    def __init__(self, start: datetime.date, end: datetime.date):
        if start > end:
            raise ValueError(
                f"Startdatum '{str(start)}' muss vor Enddatum '{str(end)}' liegen."
            )
        self._start = start
        self._end = end

    @staticmethod
    def create_from_strings(start: str, end: str) -> "DateRange":
        # In case no year is given, ensure that dates are "in order".
        current_year = datetime.date.today().year
        return DateRange(
            convert_string_to_date(start, default_year=current_year - 1),
            convert_string_to_date(end, default_year=current_year),
        )

    @staticmethod
    def create_from_string(
        range_string: str, conjunction: str = DEFAULT_CONJUNCTION
    ) -> "DateRange":
        start, end = DateRange._extract_start_and_end_date_strings(
            range_string, conjunction
        )
        return DateRange.create_from_strings(start, end)

    def __iter__(self):
        current_date = self._start
        while current_date <= self._end:
            tmp = current_date
            current_date += datetime.timedelta(days=1)
            yield tmp

    def __str__(self) -> str:
        return self.to_string()

    def __eq__(self, other: object) -> bool:
        if isinstance(other, DateRange):
            return self._start == other._start and self._end == other._end
        else:
            return NotImplemented

    def to_string(
        self,
        conjunction: str = DEFAULT_CONJUNCTION,
        date_format: str = create_date_format(),
    ) -> str:
        return DateRange._normalize_conjunction(conjunction).join(
            [
                format_date(self._start, format=date_format, locale=_LOCALE),
                format_date(self._end, format=date_format, locale=_LOCALE),
            ]
        )

    @staticmethod
    def _normalize_conjunction(conjunction: str) -> str:
        conjunction = conjunction.strip()
        conjunction_length = len(conjunction)
        return conjunction.center(conjunction_length + 2)

    @staticmethod
    def _extract_start_and_end_date_strings(
        range_string: str, conjunction: str
    ) -> Tuple[str, str]:
        if conjunction not in range_string:
            raise ValueError(
                f"Erwartetes Format für Datumsspanne ist "
                f"DD.MM.YYYY{conjunction}DD.MM.YYYY."
                f"Erhaltenen: '{range_string}'"
            )
        dates = range_string.split(conjunction)
        if not len(dates) == 2:
            raise ValueError(
                f"Erwartetes Format für Datumsspanne ist "
                f"DD.MM.YYYY{conjunction}DD.MM.YYYY"
                f"Erhaltenen: '{range_string}'"
            )
        start, end = dates
        return start.strip(), end.strip()


class DateList:
    _dates: List[datetime.date]

    def __init__(
        self, ranges: List[DateRange] = None, dates: List[datetime.date] = None
    ):
        self._dates = []

        if ranges is None:
            ranges = []
        for single_range in ranges:
            self.add_date_range(single_range)

        if dates is None:
            dates = []
        self.add_dates(dates)
        self._normalize()

    def __iter__(self):
        for date in self._dates:
            yield date

    def __len__(self) -> int:
        return len(self._dates)

    def contains(self, date: datetime.date) -> bool:
        return date in self._dates

    def add_date(self, date: datetime.date) -> None:
        self._dates.append(date)
        self._normalize()

    def add_dates(self, dates: Iterable[datetime.date]) -> None:
        self._dates.extend(dates)
        self._normalize()

    def add_date_range(self, date_range: DateRange) -> None:
        self.add_dates([date for date in date_range])
        self._normalize()

    def remove_date(self, date: datetime.date) -> None:
        try:
            self._dates.remove(date)
        except ValueError:
            pass

    def remove_dates(self, dates: Iterable[datetime.date]) -> None:
        for date in dates:
            self.remove_date(date)

    def remove_date_range(self, date_range: DateRange):
        self.remove_dates([date for date in date_range])

    def _normalize(self):
        self._dates = self._filter_duplicates(self._dates)
        self._dates.sort()

    @staticmethod
    def _filter_duplicates(
        list_with_duplicates: List[datetime.date],
    ) -> List[datetime.date]:
        unique_entries = set(list_with_duplicates)
        return list(unique_entries)


def date_list_to_ranges_of_dates(date_list: DateList) -> List[DateRange]:
    ranges: List[DateRange] = []
    if len(date_list) == 0:
        return ranges

    list_of_dates = [date for date in date_list]
    current_begin = list_of_dates[0]
    previous_date = list_of_dates[0]
    for date in list_of_dates[1:]:
        if date != previous_date + datetime.timedelta(days=1):
            ranges.append(DateRange(current_begin, previous_date))
            current_begin = date
        previous_date = date

    ranges.append(DateRange(current_begin, previous_date))
    return ranges


def load_date_ranges_from_file(file_path: Path) -> List[DateRange]:
    result_ranges = []
    with file_path.open(mode="r") as file_handle:
        date_ranges = file_handle.readlines()
        for date_range in date_ranges:
            result_ranges.append(DateRange.create_from_string(date_range))
    return result_ranges
