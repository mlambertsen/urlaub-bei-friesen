from paramiko import SSHClient, ssh_exception

from urlaub_bei_friesen.common.ssh import connect
from urlaub_bei_friesen.config.backend.ssh_ui import SshUi


def connect_requesting_credentials(ui: SshUi) -> SSHClient:
    error_message = ""
    while True:
        credentials = ui.request_user_credentials(error_message)
        try:
            ssh = connect(
                host=credentials.host,
                user=credentials.user,
                password=credentials.password,
            )
        except ssh_exception.AuthenticationException:
            error_message = (
                "Verbindung konnte nicht hergestellt werden. "
                "Bitte nochmals versuchen."
            )
            continue
        break
    return ssh
