import traceback

import PySimpleGUI as sg

import urlaub_bei_friesen.common.colors as colors
from urlaub_bei_friesen.common.icon import ICON_BASE_64

from .backend.occupancy import Occupancy
from .backend.prices import Prices
from .backend.ssh import connect_requesting_credentials
from .backend.update import check_for_updates
from .frontend.main_ui import MainUiSimpleGui
from .frontend.occupancy_config_ui import OccupancyConfigUiSimpleGui
from .frontend.prices_config_ui import PricesConfigUiSimpleGui
from .frontend.ssh_ui import SshUiPySimpleGui
from .frontend.update_ui import UpdateUiPySimpleGui

URLAUB_BEI_FRIESEN_SCHEME_NAME = "UrlaubBeiFriesenScheme"
URLAUB_BEI_FRIESEN_SCHEME = {
    "BACKGROUND": colors.MAIN_COLOR_LIGHT,
    "TEXT": colors.SECOND_COLOR_DARK,
    "INPUT": colors.TEXT_MARKED_BACKGROUND_COLOR,
    "TEXT_INPUT": colors.TEXT_MARKED_COLOR,
    "SCROLL": colors.MAIN_COLOR_DARK,
    "BUTTON": (colors.MAIN_COLOR_LIGHT, colors.SECOND_COLOR_DARK),
    "PROGRESS": (colors.SECOND_COLOR_DARK, colors.MAIN_COLOR_LIGHT),
    "BORDER": 1,
    "SLIDER_DEPTH": 0,
    "PROGRESS_DEPTH": 0,
}


def main():
    try:
        sg.theme_add_new(URLAUB_BEI_FRIESEN_SCHEME_NAME, URLAUB_BEI_FRIESEN_SCHEME)
        sg.theme(URLAUB_BEI_FRIESEN_SCHEME_NAME)
        check_for_updates(UpdateUiPySimpleGui())
        ssh = connect_requesting_credentials(SshUiPySimpleGui())
        occupancy = OccupancyConfigUiSimpleGui(Occupancy(ssh))
        prices = PricesConfigUiSimpleGui(Prices(ssh))
        ui = MainUiSimpleGui()
        ui.add_config(occupancy)
        ui.add_config(prices)
        ui.start()
    except Exception:
        sg.popup_ok(
            f"Ein interner Fehler ist aufgetreten. Bitte den Fehler "
            f"an den Maintainer des Programms melden:\n\n"
            f"{traceback.format_exc()}",
            title="Unerwarteter Fehler",
            icon=ICON_BASE_64,
            line_width=230,
        )


if __name__ == "__main__":
    main()
