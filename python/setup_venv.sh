#!/usr/bin/env bash

set -e

SCRIPT_DIR=`dirname "$(readlink -f "$0")"`
VENV_DIR="$(readlink -fm ~/.virtualenvs/urlaub_bei_friesen)"

echo "#################################"
echo "Setup venv for urlaub_bei_friesen"
echo "#################################"
echo "Script directory: ${SCRIPT_DIR}"
echo "Virtualenv directory: ${VENV_DIR}"
echo

# Check for virtualenv
if ! [ -x "$command -v virtualenv" ]; then
  echo "Install virtualenv: sudo apt install virtualenv"
fi

# Create venv
echo -n "Checking for existing virtualenv ... "
if [ ! -f "${VENV_DIR}/bin/activate" ]; then
  mkdir -p "${VENV_DIR}"
  virtualenv "$VENV_DIR" -p /usr/bin/python3
  echo "created new venv"
else
  echo "venv exists"
fi

# Activate venv
echo -n "Activate venv ... "
. "${VENV_DIR}/bin/activate"
echo "done"

# Update pip/setuptools/pip-tools
echo "Installing dependencies..."
pip install -U pip setuptools pip-tools

# Install dependencies
cd "${SCRIPT_DIR}"
pip install --find-links wheels -r ./requirements.txt

# Install local dependencies
# cd into project root path, such that paths in requirements.local can be set from there
cd "${SCRIPT_DIR}"/../
pip install -r ./python/requirements.local -v --force
