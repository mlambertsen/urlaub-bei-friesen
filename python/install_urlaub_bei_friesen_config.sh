#!/usr/bin/env bash

set -e

SCRIPT_DIR=`dirname "$(readlink -f "$0")"`
VENV_DIR="$(readlink -fm ~/.virtualenvs/urlaub_bei_friesen)"

echo "#################################"
echo "Install urlaub_bei_friesen config"
echo "#################################"
echo "Script directory: ${SCRIPT_DIR}"
echo

echo "Setting up virtualenv..."
./setup_venv.sh
echo "Finished setting up virtualenv."

echo "Install desktop entry..."
cat > ${HOME}/.local/share/applications/urlaub_bei_friesen.desktop << EOL
[Desktop Entry]
Name=Urlaub bei Friesen
Exec=bash -c "source ${VENV_DIR}/bin/activate; urlaub_bei_friesen_config"
Icon=${SCRIPT_DIR}/../content/common/images/favicon.png
Type=Application
Terminal=false
Categories=Network
MimeType=text/plain
EOL
echo "Installing desktop entry finished."
