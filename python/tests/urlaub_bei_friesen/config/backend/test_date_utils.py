import datetime
import pytest

import urlaub_bei_friesen.config.backend.date_utils as date_utils


def test_convert_string_to_date_accepts_dd_mm_yyyy():
    assert date_utils.convert_string_to_date("08.07.2016") == datetime.date(
        year=2016, month=7, day=8
    )


def test_convert_string_to_date_accepts_german_months():
    assert date_utils.convert_string_to_date("08. Juli 2016") == datetime.date(
        year=2016, month=7, day=8
    )


def test_convert_string_to_date_defaults_year_if_not_passed():
    assert date_utils.convert_string_to_date("08. Juli") == datetime.date(
        year=datetime.date.today().year, month=7, day=8
    )
    assert date_utils.convert_string_to_date("08.07.") == datetime.date(
        year=datetime.date.today().year, month=7, day=8
    )


def test_convert_string_to_date_fails_with_error_in_case_of_strange_input():
    with pytest.raises(ValueError):
        date_utils.convert_string_to_date("8. Juli/August 2021")


def test_date_range_construction():
    date_utils.DateRange(datetime.date(2018, 2, 23), datetime.date(2020, 9, 12))
    date_utils.DateRange.create_from_strings("23.2.2018", "12.9.2020")
    date_utils.DateRange.create_from_string("23.2.2018 - 12.9.2020")
    date_utils.DateRange.create_from_string("23.2.2018-12.9.2020")


def test_date_range_assures_start_not_after_end():
    with pytest.raises(ValueError):
        date_utils.DateRange(datetime.date(2020, 9, 12), datetime.date(2018, 2, 23))
    with pytest.raises(ValueError):
        date_utils.DateRange.create_from_strings("12.9.2020", "23.2.2018")
    with pytest.raises(ValueError):
        date_utils.DateRange.create_from_string("12.9.2020 - 23.2.2018")


def test_date_range_construction_via_single_string_format_expectations():
    with pytest.raises(ValueError):
        date_utils.DateRange.create_from_string("12.9.2020:23.2.2018")
    with pytest.raises(ValueError):
        date_utils.DateRange.create_from_string("12-9-2020 - 23-2-2018")


def test_date_range_can_iterate():
    date_ranges = [
        date_utils.DateRange.create_from_strings("8.7.2016", "11.07.2016"),
        date_utils.DateRange.create_from_string("8.7.2016 - 11.07.2016"),
        date_utils.DateRange.create_from_string("8.7.2016-11.07.2016"),
    ]
    expected_list = [
        datetime.date(2016, 7, 8),
        datetime.date(2016, 7, 9),
        datetime.date(2016, 7, 10),
        datetime.date(2016, 7, 11),
    ]

    for date_range in date_ranges:
        date_list = [date for date in date_range]
        assert date_list == expected_list


def test_date_range_to_string_strips_conjunctions_and_places_one_space_around_it():
    date = date_utils.DateRange.create_from_strings("8.7.2016", "23.2.2018")
    expected_str = "08.07.2016 to 23.02.2018"
    assert date.to_string(conjunction="   to      ") == expected_str


def test_date_range_to_string_allows_dropping_the_year():
    date = date_utils.DateRange.create_from_strings("8.7.2016", "23.2.2018")
    expected_str = "08.07 - 23.02"
    assert (
        date.to_string(date_format=date_utils.create_date_format(include_year=False))
        == expected_str
    )


def test_date_range_to_string_can_print_months_as_german_text():
    date = date_utils.DateRange.create_from_strings("8.7.2016", "23.2.2018")
    expected_str = "08. Juli 2016 - 23. Februar 2018"
    assert (
        date.to_string(date_format=date_utils.create_date_format(month_as_text=True))
        == expected_str
    )


def test_date_range_to_string_month_as_text_but_no_year():
    date = date_utils.DateRange.create_from_strings("8.7.2016", "23.2.2018")
    expected_str = "08. Juli - 23. Februar"
    assert (
        date.to_string(
            date_format=date_utils.create_date_format(
                month_as_text=True, include_year=False
            )
        )
        == expected_str
    )


def test_date_range_string_representation():
    date = date_utils.DateRange.create_from_strings("8.7.2016", "23.2.2018")
    expected_str = "08.07.2016 - 23.02.2018"
    assert str(date) == expected_str


def test_date_range_string_representation_can_be_used_to_construct_date_range():
    date = date_utils.DateRange.create_from_strings("8.7.2016", "23.2.2018")
    date_utils.DateRange.create_from_string(str(date))


def test_date_range_can_be_compared():
    date_ranges = [
        date_utils.DateRange.create_from_string("23.02.2020 - 25.02.2020"),
        date_utils.DateRange.create_from_string("23.02.2020 - 26.02.2020"),
        date_utils.DateRange.create_from_string("22.02.2020 - 25.02.2020"),
        "random_other_object",
    ]
    for i, j in [
        (a, b) for a, _ in enumerate(date_ranges) for b, _ in enumerate(date_ranges)
    ]:
        if i == j:
            assert date_ranges[i] == date_ranges[j]
        else:
            assert date_ranges[i] != date_ranges[j]


def test_default_constructed_date_list_is_empty():
    date_list = date_utils.DateList()
    assert len(date_list) == 0


def test_date_list_add_date():
    date_list = date_utils.DateList()
    date = datetime.date(2020, 9, 12)
    date_list.add_date(date)
    assert len(date_list) == 1
    assert date_list.contains(date)


def test_date_list_add_dates():
    date_list = date_utils.DateList()
    dates = [
        datetime.date(2020, 9, 12),
        datetime.date(2018, 2, 23),
        datetime.date(2016, 7, 8),
    ]
    date_list.add_dates(dates)
    assert len(date_list) == 3
    for date in dates:
        assert date_list.contains(date)


def test_date_list_add_date_range():
    date_list = date_utils.DateList()
    date_range = date_utils.DateRange(
        datetime.date(2020, 9, 12), datetime.date(2020, 9, 16)
    )
    date_list.add_date_range(date_range)
    assert len(date_list) == 5
    for date in date_range:
        assert date_list.contains(date)


def test_date_list_remove_date():
    dates_to_keep = [datetime.date(2020, 9, 12), datetime.date(2016, 7, 8)]
    date_to_remove = datetime.date(2018, 2, 23)
    date_list = date_utils.DateList(dates=dates_to_keep + [date_to_remove])
    assert len(date_list) == 3
    date_list.remove_date(date_to_remove)
    assert len(date_list) == 2
    for date in dates_to_keep:
        assert date_list.contains(date)


def test_date_list_remove_date_noop_if_date_not_in_list():
    date_list = date_utils.DateList(dates=[datetime.date(2018, 2, 23)])
    assert len(date_list) == 1
    date_list.remove_date(datetime.date(2018, 2, 24))
    assert len(date_list) == 1


def test_date_list_remove_dates():
    date_to_keep = datetime.date(2016, 7, 8)
    dates_to_remove = [datetime.date(2020, 9, 12), datetime.date(2018, 2, 23)]
    date_list = date_utils.DateList(dates=[date_to_keep] + dates_to_remove)
    assert len(date_list) == 3
    date_list.remove_dates(dates_to_remove)
    assert len(date_list) == 1
    assert date_list.contains(date_to_keep)


def test_date_list_remove_date_range():
    date_list = date_utils.DateList(
        ranges=[date_utils.DateRange.create_from_string("23.2.2020 - 26.2.2020")]
    )
    assert len(date_list) == 4
    date_list.remove_date_range(
        date_utils.DateRange.create_from_string("24.2.2020 - 25.2.2020")
    )
    assert len(date_list) == 2
    assert date_list.contains(datetime.date(2020, 2, 23))
    assert date_list.contains(datetime.date(2020, 2, 26))


# TODO: Add tests that list is ordered after every add operation
def test_date_list_holds_ordered_list():
    date_list = date_utils.DateList(
        ranges=[date_utils.DateRange.create_from_string("23.2.2018 - 26.2.2018")],
        dates=[datetime.date(2020, 9, 12), datetime.date(2016, 7, 8)],
    )
    expected_dates = [
        datetime.date(2016, 7, 8),
        datetime.date(2018, 2, 23),
        datetime.date(2018, 2, 24),
        datetime.date(2018, 2, 25),
        datetime.date(2018, 2, 26),
        datetime.date(2020, 9, 12),
    ]
    assert [date for date in date_list] == expected_dates


def test_date_list_to_ranges_of_dates_empty_list():
    date_list = date_utils.DateList()
    assert len(date_utils.date_list_to_ranges_of_dates(date_list)) == 0


def test_date_list_to_ranges_of_dates_list_with_one_element():
    date_list = date_utils.DateList(dates=[datetime.date(2020, 9, 12)])
    expected = [date_utils.DateRange.create_from_string("12.9.2020-12.9.2020")]
    assert date_utils.date_list_to_ranges_of_dates(date_list) == expected


def test_date_list_to_ranges_of_dates():
    date_list = date_utils.DateList(
        ranges=[date_utils.DateRange.create_from_string("18.08.1987 - 28.08.1987")]
    )
    date_list.remove_date(datetime.date(1987, 8, 19))
    date_list.remove_date(datetime.date(1987, 8, 23))
    date_list.remove_date(datetime.date(1987, 8, 27))
    expected_ranges = [
        date_utils.DateRange.create_from_string("18.08.1987 - 18.08.1987"),
        date_utils.DateRange.create_from_string("20.08.1987 - 22.08.1987"),
        date_utils.DateRange.create_from_string("24.08.1987 - 26.08.1987"),
        date_utils.DateRange.create_from_string("28.08.1987 - 28.08.1987"),
    ]
    assert date_utils.date_list_to_ranges_of_dates(date_list) == expected_ranges
