<?php
	header("Content-type: text/css");
?>

#anfahrt_auto_links
{
	text-align: center;
}

#anfahrt_auto_rechts
{
	text-align: center;
}

#anfahrt_bahn_links
{
	text-align: center;
}

#anfahrt_bahn_mitte
{
	text-align: center;
}

#anfahrt_bahn_rechts
{
	text-align: center;
}

#anfahrt_flugzeug
{
	width: 100%;
	float: left;
	text-align: center;
}


/* Nebeneinander fuer grosse Bildschirme */
@media (min-width: 580px)
{
	#anfahrt_auto_links
	{
		width: 50%;
		float: left;
	}

	#anfahrt_auto_rechts
	{
		width: 50%;
		float: left;
		padding-top: 15px;
		padding-top: 0.9375rem;
		margin-bottom: 40px;
		margin-bottom: 2.5rem;
	}

	#anfahrt_bahn_links
	{
		width: 33%;
		float: left;
		padding-top: 20px;
		padding-top: 1.25rem;
	}

	#anfahrt_bahn_mitte
	{
		width: 33%;
		float: left;
		padding-top: 15px;
		padding-top: 0.9375rem;
	}

	#anfahrt_bahn_rechts
	{
		width: 33%;
		float: left;
	}	
}


/* Untereinander bei kleinen Bildschirmen */
@media (max-width: 579px)
{
	#anfahrt_auto_links
	{
		width: 100%;
		margin-bottom: 25px;
		margin-bottom: 1.5625rem;
	}

	#anfahrt_auto_rechts
	{
		width: 100%;
		margin-bottom: 25px;
		margin-bottom: 1.5625rem;
	}

	#anfahrt_bahn_links
	{
		width: 100%;
		margin-bottom: 25px;
		margin-bottom: 1.5625rem;
	}

	#anfahrt_bahn_mitte
	{
		width: 100%;
		margin-bottom: 25px;
		margin-bottom: 1.5625rem;
	}

	#anfahrt_bahn_rechts
	{
		width: 100%;
	}	
}