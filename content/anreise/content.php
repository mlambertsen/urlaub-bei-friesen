<article class="inhalt_artikel">
	<header>
		<h3>Anreise nach F&ouml;hr</h3>
	</header>
	<p>
		F&ouml;hr ist die gr&ouml;&szlig;te deutsche Insel ohne direkte Landanbindung. Daher kann man mit dem Auto oder dem Zug nur bis Dageb&uuml;ll Mole fahren und benutzt ab dort die F&auml;hre. <!--F&ouml;hr besitzt allerdings auch einen kleinen Flughafen, der es erlaubt aus einigen St&auml;dten zu fliegen.-->
	</p>
	<header>
		<h5>Mit dem Auto</h5>
	</header>
	<p>
	Hier gibt es grunds&auml;tzlich zwei M&ouml;glichkeiten: Das Auto mit auf die F&auml;hre nehmen oder es in Dageb&uuml;ll f&uuml;r die Dauer des Urlaubes parken.
	</p>
	<a href="http://faehre.de" id="anfahrt_auto_links" target="_blank"><img src="anreise/images/faehre.de.png" alt="fahre.de"></a>
	<a href="http://inselparkplatz.de/" id="anfahrt_auto_rechts" target="_blank"><img src="anreise/images/inselparkplatz.png" alt="inselparkplatz.de"></a>
	<header>
		<h5>Mit dem Zug</h5>
	</header>
	<p>
		Der Zug f&auml;hrt bis auf die Mole in Dageb&uuml;ll. Die F&auml;hrfahrkarten kann man entweder direkt mit dem Bahnticket oder im Zug von Nieb&uuml;ll nach Dageb&uuml;ll (sofern man im Wagen der NEG sitzt) oder noch auf der Mole erwerben.
	</p>
	<div id="anfahrt_bahn_links">
		<div id="db-1434210741271"><a target="_blank" href="http://www.reiseauskunft.bahn.de/bin/query.exe/dn?Z=Wyk+auf+F%C3%B6hr+Hafen&amp;dbkanal_004=L01_S01_D001_KPK0064_Urlaub-bei-Friesen-de_LZ03" style=" width: 120px; height: 40px; display: inline-block; position: relative; text-decoration: none; cursor: pointer; margin: 0; padding: 0; zoom: 1;" title="Deutsche Bahn"><span style=" display: block; margin: 0; padding: 0; position: relative; line-height: 0; overflow: visible;"><img style=" width: 100%; margin: 0; padding: 0; display: block;" src="https://www.bahn.de/wmedia/view/mdb/media/intern/anreisebutton/design/design1-2-np-de.png" alt="bahn.de"></span></a></div>
	</div>
	<a href="http://neg-niebuell.de" id="anfahrt_bahn_mitte" target="_blank"><img src="anreise/images/neg.png" alt="neg-niebuell.de"></a>
		<a href="http://faehre.de" id="anfahrt_bahn_rechts" target="_blank"><img src="anreise/images/faehre.de.png" alt="fahre.de"></a>
	<header>
		<h5>Mit dem Flugzeug</h5>
	</header>
	<p>
		Von z.B. Hamburg, Berlin, D&uuml;sseldorf, Billund, Kopenhagen... zum Flugplatz in Wyk auf F&ouml;hr (Am S&uuml;dstrand // 25938 Wyk auf F&ouml;hr // Telefon: 04681-8139). Den Tower erreichen Sie unter 04681-5504
	</p>
	<div id="anfahrt_flugzeug">
		<a href="http://westkuestenflug.de" target="_blank"><img src="anreise/images/westkuestenflug.de.png" alt="westkuestenflug.de"></a>
	</div>
</article>

<article class="inhalt_artikel">
	<header>
		<h3>Vom Hafen zur Wohnung</h3>
	</header>
	<p>
		Wenn Sie mit dem Zug kommen oder Ihr Auto auf dem Festland stehen lassen, holen wir Sie gerne direkt am Hafen ab. Als besonderen Service legen wir f&uuml;r Sie einen Zwischenstopp beim Supermarkt ein, damit Sie Ihren Ersteinkauf t&auml;tigen k&ouml;nnen.
	</p>
	<p>
		Mit dem eigenen Auto folgen Sie einfach der Stra&szlig;e in Richtung der Inseld&ouml;rfer. Durch Nieblum und Goting gelangen Sie zu Ihrem Urlaubsort Borgsum. Dort einfach die zweite Stra&szlig;e links und schon sind Sie da.
	</p>
</article>

