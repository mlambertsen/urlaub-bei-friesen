<?php
	header("Content-type: text/css");

	require_once("colors.php");

?>

/* Lade Google-Fonts */
@import url(https://fonts.googleapis.com/css?family=Lora:400,400italic,700);
@import url(https://fonts.googleapis.com/css?family=Noto+Sans+KR:400,400italic,700);

/* Darstellung von html5-Elementen im IE8 */
header,footer,nav
{
	display: block;
}


/******************************************/
/*      Für alle Bildschirmgrößen         */
/******************************************/

/* Allgemeine Objekte */

body
{
	background-color: <?php echo $main_color_light?>;
	text-align: left;
	font-size: 100%;
	margin: 0;
}

::-moz-selection
{
	background-color: <?php echo $text_marked_background?>;
	color: <?php echo $text_marked?>;
}

::selection
{
	background-color: <?php echo $text_marked_background?>;
	color: <?php echo $text_marked?>;
}

a:link, a:visited
{
	color: <?php echo $main_color_light?>;
	font-family: 'Noto Sans KR', sans-serif;
	text-decoration: none;
}

a:hover, a:active
{
	color: <?php echo $main_color_dark?>;
}

p
{
	color: <?php echo $text_color?>;
	font-family: 'Lora', serif;
	-moz-hyphens: auto;
	-o-hyphens: auto;
	-webkit-hyphens: auto;
	-ms-hyphens: auto;
	hyphens: auto;
	font-size: 15px;
	font-size: 0.9375rem;
	line-height: 24px;
	line-height: 1.5rem;
}

ul
{
	font-family: 'Lora', serif;
	-moz-hyphens: auto;
	-o-hyphens: auto;
	-webkit-hyphens: auto;
	-ms-hyphens: auto;
	hyphens: auto;
	color: <?php echo $text_color?>;
}

h1
{
	color: <?php echo $main_color_light?>;
	margin-top: 0;
	margin-bottom: 15px;
	margin-bottom: 0.9375rem;
	padding: 0;
	font-family: 'Noto Sans KR', sans-serif;
	font-size: 25px;
	font-size: 1.5625rem;
}

h3
{
	color: <?php echo $main_color_dark?>;
	margin-bottom: 15px;
	margin-bottom: 0.9375rem;
	margin-top: 0;
	font-family: 'Noto Sans KR', sans-serif;
	font-size: 20px;
	font-size: 1.25rem;
}

h5
{
	color: <?php echo $main_color_dark?>;
	margin-bottom: 10px;
	margin-bottom: 0.625rem;
	margin-top: 0;
	font-family: 'Noto Sans KR', sans-serif;
	font-size: 15px;
	font-size: 0.9375rem;
}

td
{
	color: <?php echo $text_color?>;
	font-family: 'Lora', serif;
	font-size: 15px;
	font-size: 0.9375rem;
	-moz-hyphens: auto;
	-o-hyphens: auto;
	-webkit-hyphens: auto;
	-ms-hyphens: auto;
	hyphens: auto;
	vertical-align: center;
}

th
{
	color: <?php echo $text_color?>;
	font-family: 'Lora', serif;
	font-size: 15px;
	font-size: 0.9375rem;
	-moz-hyphens: auto;
	-o-hyphens: auto;
	-webkit-hyphens: auto;
	-ms-hyphens: auto;
	hyphens: auto;
	vertical-align: center;
}

.button
{
	appearance: none;
	-webkit-appearance: none;
	-moz-appearance: none;
	background-color: <?php echo $main_color_dark?>;
	-moz-border-radius: 42px;
	-webkit-border-radius: 42px;
	border-radius: 42px;
	-moz-border-radius: 2.625rem;
	-webkit-border-radius: 2.625rem;
	border-radius: 2.625rem;
	display: inline-block;
	cursor: pointer;
	color: <?php echo $main_color_light?>;
	font-family: 'Noto Sans KR', sans-serif;
	font-size: 15px;
	font-size: 0.9375rem;
	font-weight: bold;
	padding: 2px 8px;
	padding: 0.125rem 0.5rem;
	text-decoration: none;
	text-shadow: 0px 1px 0px <?php echo $text_color?>;
}

.button:hover, .button:active
{
	background-color: <?php echo $text_color?>;
}


/* Mittlere Umgebung */
#rahmen
{
	background-color: transparent;
	margin: 0 auto;
}


/* Navigation/Menü */

nav
{
	display: none;
	background-color: <?php echo $second_color_dark?>;
}

#no_script_nav
{
	display: block;
}

nav ul
{
	list-style: none;
	margin: 0;
	padding: 0;	
}

nav ul li
{
	line-height: 32px;
	line-height: 2rem;
}

nav a:link, nav a:visited
{
	font-size: 18px;
	font-size: 1.125rem;
	font-weight: bold;
}


/* Inhalt allgemein */

#inhalt
{
	width: 96%;
	height: auto;
	padding: 15px 2% 1px 2%;
	padding: 0.9375rem 2% 0.9375rem 2%;
	background-color: <?php echo $main_color_dark?>;
}

#inhalt a:link, #inhalt a:visited
{
	display: inline-block;
	color: <?php echo $main_color_light?>;
}

#inhalt a:hover, #inhalt a:active
{
	color: <?php echo $second_color_dark?>;
}

.inhalt_artikel
{
	width: 94%;
	margin-bottom: 15px;
	margin-bottom: 0.9375rem;
	padding: 15px 3% 15px 3%;
	padding: 0.9375rem 3% 0.9375rem 3%;
	background-color: <?php echo $main_color_light?>;
	overflow: hidden;

	border-radius: 8px;
	-moz-border-radius: 8px;
	-webkit-border-radius: 8px;
	border-radius: 0.5rem;
	-moz-border-radius: 0.5rem;
	-webkit-border-radius: 0.5rem;
}

.inhalt_artikel ul
{
	color: <?php echo $text_color?>;
	font-family: 'Lora', serif;
	-moz-hyphens: auto;
	-o-hyphens: auto;
	-webkit-hyphens: auto;
	-ms-hyphens: auto;
	hyphens: auto;
	font-size: 15px;
	font-size: 0.9375rem;
	line-height: 24px;
	line-height: 1.5rem;
}

.ul_checkmark
{
	list-style-image: url('../images/checkmark.svg');
}

.ul_pfeil
{
	list-style-image: url('../images/pfeil.svg');
}

#inhalt .inhalt_artikel a:link, #inhalt .inhalt_artikel a:visited
{
	display: inline-block;
	color: <?php echo $second_color_dark?>;
}

#inhalt .inhalt_artikel a:hover, #inhalt .inhalt_artikel a:active
{
	color: <?php echo $main_color_dark?>;
}

.hide
{
	visibility: hidden;
}

.no_display
{
	display: none;
}

.align_right
{
	text-align:right;
}

.align_center
{
	text-align: center;
}

.hinweis
{
	color: <?php echo $alert_color?>;
}


/* Footer */

.main_footer
{
	width: 100%;
	line-height: 32px;
	line-height: 2rem;
	font-size: 16px;
	font-size: 1.0rem;
	font-weight: bold;
	text-align: right;
}

.main_footer a:link, .main_footer a:visited
{
	color: <?php echo $main_color_light?>;
	padding: 0 3% 2px 3%;
	padding: 0 3% 0.125rem 3%;
}

.main_footer a:hover, .main_footer a:active
{
	color: <?php echo $main_color_dark?>;
}



/* Font Größe (skaliert die ganze Seite) */
@media (max-width: 869px){html{font-size: 14px;}}
@media (min-width: 870px) and (max-width: 914px){html{font-size: 14px;}}
@media (min-width: 915px) and (max-width: 979px){html{font-size: 15px;}}
@media (min-width: 980px) and (max-width: 1379px){html{font-size: 16px;}}
@media (min-width: 1380px) and (max-width: 1799px){html{font-size: 17px;}}
@media (min-width: 1800px){html{font-size: 18px;}}


/******************************************/
/*          Große Bildschirme             */
/******************************************/

@media (min-width: 870px)
{


	/* Allgemeine Objekte */

	body
	{
		background: url(../images/wattenmeer.jpg);
		background-repeat:no-repeat;
		background-attachment:fixed;
		background-position: top center;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
	}


	/* Mittlere Umgebung */

	#rahmen
	{
		max-width: 960px;
		max-width: 60rem;
	}



	/* Header */

	.main_header
	{
		background-color: transparent;
		width: 100%;
		height: 150px;
		height: 9.375rem;
		margin-bottom: 20px;
		margin-bottom: 1.25rem;
	}

	.header_logo
	{
		float: left;
		height: 100%;
		padding-left: 8px;
		padding-left: 0.5rem;
	}

	.header_info
	{
		float: right;
		height: 100%;
		padding-right: 8px;
		padding-right: 0.5rem;
	}

	.header_info p
	{
		color: <?php echo $second_color_dark?>;
		text-align: right;
		font-size: 16px;
		font-size: 1.0rem;
	}

	.header_info a:link, .header_info a:visited
	{
		color: <?php echo $main_color_dark?>;
	}

	.header_info a:hover, .header_info a:active
	{
		color: <?php echo $second_color_dark?>;
	}

	#noscript_small_device_header
	{
		display: none;
	}

	.header_small_device
	{
		display: none;
	}


	/* Navigation/Menü */

	nav
	{
		width: 96%;
		padding: 0 2%;

		border-radius: 8px;
		-moz-border-radius: 8px;
		-webkit-border-radius: 8px;
		border-radius: 0.5rem;
		-moz-border-radius: 0.5rem;
		-webkit-border-radius: 0.5rem;

		-webkit-box-shadow: 0px 0px 3px 3px <?php echo $second_color_dark?>;
		-moz-box-shadow: 0px 0px 3px 3px <?php echo $second_color_dark?>;
		box-shadow: 0px 0px 3px 3px <?php echo $second_color_dark?>;
		-webkit-box-shadow: 0px 0px 0.1875rem 0.1875rem <?php echo $second_color_dark?>;
		-moz-box-shadow: 0px 0px 0.1875rem 0.1875rem <?php echo $second_color_dark?>;
		box-shadow: 0px 0px 0.1875rem 0.1875rem <?php echo $second_color_dark?>;
	}

	nav ul li
	{
		margin: 0;
		padding: 0 0 2px 0;
		padding: 0 0 0.125rem 0;
		display: inline-block;
	}

	nav a:link, nav a:visited
	{
		padding: 0 18px 0;
		padding: 0 1.125rem 0;
	}

	.nav_active a:link, .nav_active a:visited
	{
		padding: 10px 18px 30px;
		padding: 0.625rem 1.125rem 1.875rem;
		background-color: <?php echo $main_color_dark?>;

		-webkit-box-shadow: 0px 0px 3px 3px <?php echo $main_color_dark?>;
		-moz-box-shadow: 0px 0px 3px 3px <?php echo $main_color_dark?>;
		box-shadow: 0px 0px 3px 3px <?php echo $main_color_dark?>;
		-webkit-box-shadow: 0px 0px 0.1875rem 0.1875rem <?php echo $main_color_dark?>;
		-moz-box-shadow: 0px 0px 0.1875rem 0.1875rem <?php echo $main_color_dark?>;
		box-shadow: 0px 0px 0.1875rem 0.1875rem <?php echo $main_color_dark?>;

		border-radius: 8px 8px 0px 0px;
		-moz-border-radius: 8px 8px 0px 0px;
		-webkit-border-radius: 8px 8px 0px 0px;
		border-radius: 0.5rem;
		-moz-border-radius: 0.5rem;
		-webkit-border-radius: 0.5rem;
	}

	.nav_active a:hover, .nav_active a:active
	{
		color: <?php echo $second_color_dark?>;
	}

	.empty
	{
		height: 20px;
		height: 1.25rem;
	}


	/* Inhalt allgemein */

	#inhalt
	{
		margin: 0 0 20px 0;
		margin: 0 0 1.25rem 0;

		border-radius: 8px;
		-moz-border-radius: 8px;
		-webkit-border-radius: 8px;
		border-radius: 0.5rem;
		-moz-border-radius: 0.5rem;
		-webkit-border-radius: 0.5rem;

		-webkit-box-shadow: 0px 0px 3px 3px <?php echo $main_color_dark?>;
		-moz-box-shadow: 0px 0px 3px 3px <?php echo $main_color_dark?>;
		box-shadow: 0px 0px 3px 3px <?php echo $main_color_dark?>;
		-webkit-box-shadow: 0px 0px 0.1875rem 0.1875rem <?php echo $main_color_dark?>;
		-moz-box-shadow: 0px 0px 0.1875rem 0.1875rem <?php echo $main_color_dark?>;
		box-shadow: 0px 0px 0.1875rem 0.1875rem <?php echo $main_color_dark?>;
	}


	/* Footer */

	.main_footer
	{
		margin-bottom: 50px;
		margin-bottom: 3.125rem;
		background-color: transparent;
	}

	.main_footer a:link, .main_footer a:visited
	{
		display: inline;
		background-color: <?php echo $second_color_dark?>;

		border-radius: 8px;
		-moz-border-radius: 8px;
		-webkit-border-radius: 8px;
		border-radius: 0.5rem;
		-moz-border-radius: 0.5rem;
		-webkit-border-radius: 0.5rem;

		-webkit-box-shadow: 0px 0px 3px 3px <?php echo $second_color_dark?>;
		-moz-box-shadow: 0px 0px 3px 3px <?php echo $second_color_dark?>;
		box-shadow: 0px 0px 3px 3px <?php echo $second_color_dark?>;
		-webkit-box-shadow: 0px 0px 0.1875rem 0.1875rem <?php echo $second_color_dark?>;
		-moz-box-shadow: 0px 0px 0.1875rem 0.1875rem <?php echo $second_color_dark?>;
		box-shadow: 0px 0px 0.1875rem 0.1875rem <?php echo $second_color_dark?>;
	}

}


/******************************************/
/*         Mittlere Bildschirme           */
/******************************************/

@media (min-width: 540px) and (max-width: 869px)
{

	/* Allgemeine Objekte */

	body
	{
		background: url(../images/wattenmeer.jpg);
		background-repeat:no-repeat;
		background-attachment:fixed;
		background-position: top center;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
	}


	/* Mittlere Umgebung */

	#rahmen
	{
		max-width: 100%;
	}


	/* Header */

	.main_header
	{
		background-color: transparent;
		width: 100%;
		height: 150px;
		height: 9.375rem;
		margin-bottom: 20px;
		margin-bottom: 1.25rem;
	}

	.header_logo
	{
		float: left;
		height: 100%;
		padding-left: 8px;
		padding-left: 0.5rem;
	}

	.header_info
	{
		float: right;
		height: 100%;
		padding-right: 8px;
		padding-right: 0.5rem;
	}

	.header_info p
	{
		color: <?php echo $second_color_dark?>;
		text-align: right;
		font-size: 16px;
		font-size: 1.0rem;
	}

	.header_info a:link, .header_info a:visited
	{
		color: <?php echo $main_color_dark?>;
	}

	.header_info a:hover, .header_info a:active
	{
		color: <?php echo $second_color_dark?>;
	}

	#noscript_small_device_header
	{
		display: none;
	}

	.header_small_device
	{
		display: none;
	}


	/* Navigation/Menü */

	nav
	{
		width: 100%;
		padding: 0;

		border-radius: 8px;
		-moz-border-radius: 8px;
		-webkit-border-radius: 8px;
		border-radius: 0.5rem;
		-moz-border-radius: 0.5rem;
		-webkit-border-radius: 0.5rem;
	}

	nav ul li
	{
		width: 24%;
		width: calc(25% - 3px);
		margin: 0;
		padding: 0 0 2px 0;
		padding: 0 0 0.125rem 0;
		display: inline-block;
		text-align: center;
	}


	nav a:link, nav a:visited
	{
		width: 100%;
		padding: 0;
		display:inline-block;
	}

	.nav_active
	{
		background-color: <?php echo $main_color_dark?>;

		border-radius: 8px 8px 0px 0px;
		-moz-border-radius: 8px 8px 0px 0px;
		-webkit-border-radius: 8px 8px 0px 0px;
		border-radius: 0.5rem;
		-moz-border-radius: 0.5rem;
		-webkit-border-radius: 0.5rem;
	}

	.nav_active a:hover, .nav_active a:active
	{
		color: <?php echo $second_color_dark?>;
	}

	.empty
	{
		height: 20px;
		height: 1.25rem;
	}


	/* Inhalt allgemein */

	#inhalt
	{
		margin: 0 0 20px 0;
		margin: 0 0 1.25rem 0;

		border-radius: 8px;
		-moz-border-radius: 8px;
		-webkit-border-radius: 8px;
		border-radius: 0.5rem;
		-moz-border-radius: 0.5rem;
		-webkit-border-radius: 0.5rem;
	}


	/* Footer */

	.main_footer
	{
		margin-bottom: 50px;
		margin-bottom: 3.125rem;
		background-color: transparent;
	}

	.main_footer a:link, .main_footer a:visited
	{
		display: inline;
		background-color: <?php echo $second_color_dark?>;

		border-radius: 8px;
		-moz-border-radius: 8px;
		-webkit-border-radius: 8px;
		border-radius: 0.5rem;
		-moz-border-radius: 0.5rem;
		-webkit-border-radius: 0.5rem;
	}
}



/******************************************/
/*          Kleine Bildschirme            */
/******************************************/

@media (min-width: 350px) and (max-width: 539px)
{
	.main_header
	{
		display: none;
	}

	#noscript_small_device_header
	{
		display: block;
		text-align: center;
	}

	.header_small_device
	{
		width: 100%;
		height: 64px;
		height: 4rem;
		background-color: <?php echo $second_color_dark?>;
		cursor: pointer;
	}

	#header_logo_small_device
	{
		display: block;
		float: left;
		height: 64px;
		height: 4rem;
		padding: 0 2%;
	}

	#header_logo_tiny_device
	{
		display: none;
	}

	#header_title_small_device
	{
		position: absolute;
		width: 100%;
		text-align:center;
	}

	#header_title_small_device h3
	{
		margin: 14px 0 18px 0;
		margin: 0.87rem 0 1.125rem 0;
		color: <?php echo $main_color_dark?>;
	}

	#header_menu_small_device
	{
		float: right;
		height: 32px;
		height: 2rem;
		padding: 16px 4%;
		padding: 1rem 4%;
	}


	/* Mittlere Umgebung */

	#rahmen
	{
		max-width: 100%;
	}


	/* Navigation/Menü */

	nav
	{
		width: 100%;
		padding: 0;
	}

	nav ul
	{
		border-top-width: 1px;
		border-top-width: 0.0625rem;
		border-top-color: <?php echo $main_color_dark?>;
		border-top-style: solid;
	}

	nav ul li
	{
		display: block;
		text-align: center;
	}

	nav a:link, nav a:visited
	{
		width: 100%;
		padding: 0;
		display:inline-block;
	}

	nav a:hover, nav a:active
	{
		color: <?php echo $main_color_light?>;
		background-color: <?php echo $main_color_dark?>;
	}

	.nav_active a:link, .nav_active a:visited
	{
		color: <?php echo $main_color_dark?>;
	}

	.nav_active a:hover, .nav_active a:active
	{
		color: <?php echo $second_color_dark?>;
	}

	.empty
	{
		display: none;
	}

	/* Inhalt allgemein */

	#inhalt
	{
		margin: 0;
	}


	/* Footer */

	.main_footer
	{
		background-color: <?php echo $second_color_dark?>;
	}
}


/******************************************/
/*          Winzige Bildschirme           */
/******************************************/

@media (max-width: 349px)
{
	.main_header
	{
		display: none;
	}

	#noscript_small_device_header
	{
		display: block;
		text-align: center;
	}

	.header_small_device
	{
		width: 100%;
		height: 64px;
		height: 4rem;
		background-color: <?php echo $second_color_dark?>;
		cursor: pointer;
	}

	#header_logo_small_device
	{
		display: none;
	}

	#header_logo_tiny_device
	{
		display: block;
		float: left;
		height: 64px;
		height: 4rem;
		padding: 0 2%;
	}

	#header_title_small_device
	{
  		position: absolute;
  		width: 100%;
		text-align:center;
	}

	#header_title_small_device h3
	{
		margin: 14px 0 18px 0;
		margin: 0.87rem 0 1.125rem 0;
		color: <?php echo $main_color_dark?>;
	}

	#header_menu_small_device
	{
		float: right;
		height: 32px;
		height: 2rem;
		padding: 16px 4%;
		padding: 1rem 4%;
	}


	/* Mittlere Umgebung */

	#rahmen
	{
		max-width: 100%;
	}


	/* Navigation/Menü */

	nav
	{
		width: 100%;
		padding: 0;
	}

	nav ul
	{
		border-top-width: 1px;
		border-top-width: 0.0625rem;
		border-top-color: <?php echo $main_color_dark?>;
		border-top-style: solid;
	}

	nav ul li
	{
		display: block;
		text-align: center;
	}

	nav a:link, nav a:visited
	{
		width: 100%;
		padding: 0;
		display:inline-block;
	}

	nav a:hover, nav a:active
	{
		color: <?php echo $main_color_light?>;
		background-color: <?php echo $main_color_dark?>;
	}

	.nav_active a:link, .nav_active a:visited
	{
		color: <?php echo $main_color_dark?>;
	}

	.nav_active a:hover, .nav_active a:active
	{
		color: <?php echo $second_color_dark?>;
	}

	.empty
	{
		display: none;
	}


	/* Inhalt allgemein */

	#inhalt
	{
		margin: 0;
	}


	/* Footer */

	.main_footer
	{
		background-color: <?php echo $second_color_dark?>;
	}
}
