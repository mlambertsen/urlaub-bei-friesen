<?php
	$main_color_light = '#fff4d5';
	$main_color_dark = '#AF9b63';
	$second_color_dark = '#925147';

	$text_color = '#3D6456';//232754';
	$text_marked = '#fff4d5';
	$text_marked_background = '#3D6456';//232754';
	$alert_color = '#925147';
?>