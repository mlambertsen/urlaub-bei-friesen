<header class="main_header">
	<div class = "header_logo">
		<a href="index.php?seite=startseite" title="Haus Lambertsen Startseite"><img style="height: 100%" src="common/images/logo.svg" alt="Haus Lambertsen Logo"></a>
	</div>
	<div class = "header_info">
		<p>
		<strong>Arfst und Karin Lambertsen</strong> <br>
		<strong>S&uuml;&uuml;derwoi 5</strong> <br>
		<strong>25938 Borgsum auf F&ouml;hr</strong>
		</p>
		<p>
		Telefon: <strong>04683-1092</strong> <br>
		Email: <strong><a href="mailto:lambertsen@borgsum.de">lambertsen@borgsum.de</a></strong>
		</p>
	</div>
</header>

<header class="header_small_device">
	<div id = "header_logo_small_device">
		<img style="height: 100%" src="common/images/logo_small_device.svg" alt="Haus Lambertsen Logo">
	</div>
	<div id = "header_logo_tiny_device">
		<img style="height: 100%" src="common/images/logo_tiny_device.svg" alt="Haus Lambertsen Logo">
	</div>
	<div id = "header_menu_small_device">
		<img style="height: 100%" src="common/images/menu.png" alt="Men&uuml;" id="menu_button_small_device">
	</div>
	<div id="header_title_small_device">
		<h3 id="title_text_small_device">
			<?php
				switch($seite){
					case "wohnungen":
						echo "Wohnungen";
						break;
					case "garten":
						echo "Garten";
						break;
					case "preise_und_angebote":
						echo "Preise/Angebote";
						break;
					case "anfrage":
						echo "Anfrage";
						break;
					case "unsere_insel":
						echo "Unsere Insel";
						break;
					case "anreise":
						echo "Anreise";
						break;
					case "kontakt":
						echo "Kontakt";
						break;
					case "impressum":
						echo "Impressum";
						break;
					default:
						echo "Startseite";
						break;
	}
			?>
		</h3>
	</div>
</header>