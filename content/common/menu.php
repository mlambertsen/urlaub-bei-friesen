<nav>
	<ul>
		<li <?php if($seite == 'startseite') echo 'class="nav_active"';?>><a href="index.php?seite=startseite" title="Startseite">Startseite</a></li>
		<li <?php if($seite == 'wohnungen') echo 'class="nav_active"';?>><a href="index.php?seite=wohnungen" title="Wohnungen">Wohnungen</a></li>
		<li <?php if($seite == 'garten') echo 'class="nav_active"';?>><a href="index.php?seite=garten" title="Garten">Garten</a></li>
		<li <?php if($seite == 'preise_und_angebote') echo 'class="nav_active"';?>><a href="index.php?seite=preise_und_angebote" title="Preise und Angebote">Preise/Angebote</a></li>
		<li <?php if($seite == 'anfrage') echo 'class="nav_active"';?>><a href="index.php?seite=anfrage" title="Anfrage">Anfrage</a></li>
		<li <?php if($seite == 'unsere_insel') echo 'class="nav_active"';?>><a href="index.php?seite=unsere_insel" title="Unsere Insel">Unsere Insel</a></li>
		<li <?php if($seite == 'anreise') echo 'class="nav_active"';?>><a href="index.php?seite=anreise" title="Anreise">Anreise</a></li>
	</ul>
</nav>
<noscript>
	<nav id="no_script_nav">
		<ul>
			<li <?php if($seite == 'startseite') echo 'class="nav_active"';?>><a href="index.php?seite=startseite" title="Startseite">Startseite</a></li>
			<li <?php if($seite == 'wohnungen') echo 'class="nav_active"';?>><a href="index.php?seite=wohnungen" title="Wohnungen">Wohnungen</a></li>
			<li <?php if($seite == 'garten') echo 'class="nav_active"';?>><a href="index.php?seite=garten" title="Garten">Garten</a></li>
			<li <?php if($seite == 'preise_und_angebote') echo 'class="nav_active"';?>><a href="index.php?seite=preise_und_angebote" title="Preise und Angebote">Preise/Angebote</a></li>
			<li <?php if($seite == 'anfrage') echo 'class="nav_active"';?>><a href="index.php?seite=anfrage" title="Anfrage">Anfrage</a></li>
			<li <?php if($seite == 'unsere_insel') echo 'class="nav_active"';?>><a href="index.php?seite=unsere_insel" title="Unsere Insel">Unsere Insel</a></li>
			<li <?php if($seite == 'anreise') echo 'class="nav_active"';?>><a href="index.php?seite=anreise" title="Anreise">Anreise</a></li>
		</ul>
	</nav>
</noscript>
<div class="empty"></div>