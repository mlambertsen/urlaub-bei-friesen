var nav_visible;

document.getElementById('header_logo_small_device')
    .addEventListener('click', toggle_menu);
document.getElementById('header_logo_tiny_device')
    .addEventListener('click', toggle_menu);
document.getElementById('header_title_small_device')
    .addEventListener('click', toggle_menu);
document.getElementById('header_menu_small_device')
    .addEventListener('click', toggle_menu);
addResizeEvent(check_nav);

$('nav').ready(function() {
  // get width of window
  var w = window.innerWidth;
  if (w > 539) {
    nav_visible = true;
    $('nav').slideToggle(0, 'linear');
  } else {
    nav_visible = false;
  };
});


function check_nav() {
  // get width of window
  var w = window.innerWidth;
  if (w > 539) {
    if (!nav_visible) {
      $('nav').slideToggle(0, 'linear');
      nav_visible = true;
    };
  } else {
    if (nav_visible) {
      $('nav').slideToggle(0, 'linear');
      nav_visible = false;
      document.getElementById('title_text_small_device').style.color =
          '#AF9b63';
      $('#menu_button_small_device')
          .attr('src', 'common/images/menu.png');
    };
  }
}

function toggle_menu() {
  if (nav_visible) {
    nav_visible = false;
    document.getElementById('title_text_small_device').style.color = '#AF9b63';
    $('#menu_button_small_device')
        .attr('src', 'common/images/menu.png');
  } else {
    nav_visible = true;
    document.getElementById('title_text_small_device').style.color = '#fff4d5';
    $('#menu_button_small_device')
        .attr('src', 'common/images/menu_active.png');
  };
  $('nav').slideToggle('slow', 'swing');
}