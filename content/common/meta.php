<?php
	function meta_title($page)
	{
		switch($page)
		{
			case "wohnungen":
				return 'Ferienwohnung Lambertsen - Wohnungen';
			case "garten":
				return 'Ferienwohnung Lambertsen - Garten';
			case "preise_und_angebote":
				return 'Ferienwohnung Lambertsen - Preise und Angebote';
			case "anfrage":
				return 'Ferienwohnung Lambertsen - Anfrage';
			case "unsere_insel":
				return 'Ferienwohnung Lambertsen - Unsere Insel';
			case "anreise":
				return 'Ferienwohnung Lambertsen - Anreise';
			case "kontakt":
				return 'Ferienwohnung Lambertsen - Kontakt';
			case "impressum":
				return 'Ferienwohnung Lambertsen - Impressum';
			default:
				return 'Ferienwohnung Lambertsen - Urlaub bei Friesen, Ferien auf der grünen Insel';
		}
	}


	function meta_description($page)
	{
		switch($page)
		{
			case "wohnungen":
				return 'Das Haus Lambertsen ist ruhig gelegen zentral auf der Insel F&ouml;hr. Die Wohnungen bestehen aus einer vollausgestatteten gro&szlig; Wohnk&uuml;che, einem gro&szlig;en Wohnzimmer mit Schrankehebett, einem gem&uuml;tlichen Schlafzimmer und einem Tageslichtbad.';
			case "garten":
				return 'Der gro&szlig;e und liebevoll gepflegte Garten des Haus Lambertsen bietet mit seinen ruhigen Sitzecken und dem hauseigenen Spielplatz f&uuml;r jeden genau das richtige.';
			case "preise_und_angebote":
				return 'Preistabelle des Haus Lambertsen. Mit 50-70&euro; ein preiswertes Angebot f&uuml;r einen der sch&ouml;nsten Ferienorte am Meer. Und selbst dieser Preis wird durch weitere Angebote noch attraktiver!';
			case "anfrage":
				return 'Haben Sie Fragen zum Angebot des Hause Lambertsen oder wollen eine Buchungsanfrage stellen? Nutzen Sie das Kontaktformular oder schicken Sie uns eine Email. Der Belegungsplan unserer Wohnungen hilft Ihnen dabei die Urlaubsreise zu planen.';
			case "unsere_insel":
				return 'Informationen zu Sehensw&uuml;rdigkeiten und Aktivit&auml;ten im Dorf Borgsum, der Insel F&ouml;hr und dem Nordfriesischen Wattenmeer, die Sie mit einem Urlaub im Haus Lambertsen erleben k&ouml;nnen.';
			case "anreise":
				return 'Informationen und Links zur Anreise auf die Insel F&ouml;hr und zu den Wohnungen im Haus Lambertsen.';
			case "kontakt":
				return 'Kontaktinformationen zur Familie Lambertsen.';
			case "impressum":
				return 'Impressum zur Homepage, Hinweise zum Urheberrecht und Haftungshinweis im Bezug auf Links von Seiten Dritter.';
			default:
				return 'Das Haus Lambertsen. Vorstellung der sch&ouml;nen Ferienwohnungen anhand eines Youtube-Videos und Auflistung der wesentlichen Vorz&uuml;ge des Hauses als Urlaubsort und Services der Vermietung.';
		}
	}


	function meta_keywords($page)
	{
		switch($page)
		{
			case "wohnungen":
				return 'insel, f\ouml;hr, nordsee, urlaub, ferien, ferienwohnung, allergiker, ruhig, zentral, wohnk&uuml;che, tageslichtbad, wohnzimmer, schlafzimmer';
			case "garten":
				return 'insel, f\ouml;hr, nordsee, urlaub, ferien, ferienwohnung, ruhig, spielplatz, liegewiese, terasse, grill, autostellplatz';
			case "preise_und_angebote":
				return 'insel, f\ouml;hr, nordsee, urlaub, ferien, ferienwohnung, fewo, g&uuml;nstig, preiswert, sonderangebot';
			case "anfrage":
				return 'insel, f\ouml;hr, nordsee, urlaub, ferien, ferienwohnung, fewo, lastminute, buchen, anfragen, belegung, kalender';
			case "unsere_insel":
				return 'insel, f\ouml;hr, nordsee, urlaub, ferien, ferienwohnung, fewo, borgsum, nordfriesland, wattenmeer, weltnaturerbe, nationalpark';
			case "anreise":
				return 'insel, f\ouml;hr, nordsee, urlaub, ferien, ferienwohnung, fewo, anreise, f&auml;hre, bahn, auto';
			case "kontakt":
				return 'insel, f\ouml;hr, nordsee, urlaub, ferien, ferienwohnung, fewo, kontakt, email, telefon, post';
			case "impressum":
				return 'insel, f\ouml;hr, nordsee, urlaub, ferien, ferienwohnung, fewo, urheberrecht, haftungshinweis, eigent&uuml;mer';
			default:
				return 'insel, f\ouml;hr, nordsee, urlaub, ferien, ferienwohnung, fewo, ruhig, zentral, strandnah, allergiker, kinderfreundlich';
		}
	}
?>