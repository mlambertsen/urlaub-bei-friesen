<?php
	function get_max_unix_timestamp($path)
	{
        $max = 0;
        $directory = new RecursiveDirectoryIterator($path);
		$file_iterator = new RecursiveIteratorIterator($directory);
		foreach ($file_iterator as $file_info) {
			if ($file_info->isFile()) {
                $max = max($max, $file_info->getMTime());
			}
        }
        return $max;
    }


    function get_latest_change_date($page)
    {
        $index_timestamp = filemtime("index.php");
        $max_general_timestamp = get_max_unix_timestamp("common");
        $max_page_timestamp = get_max_unix_timestamp($page);
        return date("Y-m-d", max($index_timestamp, $max_general_timestamp, $max_page_timestamp));
    }
?>