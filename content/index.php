<?php
	if(isset($_GET["seite"])){
		$seite = $_GET["seite"];
	}
	else{
		$seite = "startseite";
	}

	if(isset($_POST["anfrage_senden"]))
	{
		$seite = "anfrage";
		if(empty($_POST["gast_datum_anreise"]) || empty($_POST["gast_datum_abreise"]) || empty($_POST["gast_anzahl_personen"]) || empty($_POST["gast_name"]) || empty($_POST["gast_email"]))
		{
			$anfrage_gesendet = 2;
			$_SESSION['anfrage_gesendet'] = false;
		}
		else
		{
			session_start();
			if(isset($_SESSION['anfrage_gesendet']))
			{
				if($_SESSION['anfrage_gesendet'])
				{
					$anfrage_gesendet = 0;
					$_SESSION['anfrage_gesendet'] = false;
				}
				else
				{
					$anfrage_gesendet = 1;
					$_SESSION['anfrage_gesendet'] = true;
				}
			}
			else
			{
				$anfrage_gesendet = 1;
				$_SESSION['anfrage_gesendet'] = true;
			}
		}
	}
	else
	{
		$anfrage_gesendet = 0;
	}

	require_once("common/meta.php");
	require_once("common/date.php");
?>

<!DOCTYPE html>

<html lang="de-DE">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<!-- Include meta tag to ensure proper rendering and touch zooming -->
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<?php 
			echo '<title>'.meta_title($seite).'</title>';
			echo '<meta name="description" content="'.meta_description($seite).'">';
			echo '<meta name="keywords" content="'.meta_keywords($seite).'">';
		?>

		<meta name="author" content="Lambertsen">
		<meta name="date" content=<?php echo get_latest_change_date($seite)?>>
		
		<link rel="shortcut icon" href="images/common/favicon.png" type="image/png" />
		<link rel="icon" href="images/common/favicon.png" type="image/png" />
		<?php 
			echo '<link rel="stylesheet" href="common/style/style.php" type="text/css"/>';
			echo '<link rel="stylesheet" href="'.$seite.'/style.php" type="text/css"/>';
		?>
		
		<!--[if lt IE  9]>
			<script>
				document.createElement('header');
				document.createElement('nav');
				document.createElement('footer');
			</script>
		<![endif]-->
	</head>
	
	<body>
		<div id="rahmen">
			<?php require_once("common/header.php"); ?>
			<?php require_once("common/menu.php"); ?>
			<section id="inhalt">
				<?php require_once("".$seite."/content.php"); ?>
			</section>
			<?php require_once("common/footer.php"); ?>
		</div>

	<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.4.js"></script>
	<script type="text/javascript" src="common/scripts/resize.js"></script>
	<script type="text/javascript" src="common/scripts/nav.js"></script>
	<?php
		// give javascript data of calendar
		if($seite == "anfrage")
		{
			require_once("anfrage/calendar.php");
		}
	?>
	</body>

</html>
