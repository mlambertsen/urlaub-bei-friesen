<header>
	<h1 class="ueberschrift">Impressum & Datenschutz</h1>
</header>
<article class="impressum_einfuehrung">
	<p>Dieses Impressum ist g&uuml;ltig f&uuml;r die folgenden Internetadressen:</p>
	<a href="http://ferienwohnung-nordseeinsel.de" title="ferienwohnung-nordseeinsel.de">ferienwohnung-nordseeinsel.de</a>
	<br>
	<a href="http://urlaub-bei-friesen.de" title="urlaub-bei-friesen.de">urlaub-bei-friesen.de</a>
	<br>
	<a href="http://föhr-fewo.de" title="föhr-fewo.de">f&ouml;hr-fewo.de</a>
</article>
<article class="inhalt_artikel impressum_links">
	<header>
		<h3>Eigent&uuml;mer</h3>
	</header>
	<p>
		Arfst und Karin Lambertsen<br>
		S&uuml;&uuml;derwoi 5<br>
		25938 Borgsum auf F&ouml;hr
	</p>
	<p>
		Telefon: 04683-1092<br>
		Email: <a href="mailto:lambertsen@borgsum.de">lambertsen@borgsum.de</a>
	</p>
</article>
<article class="inhalt_artikel impressum_rechts">
	<header>
		<h3>Webmaster</h3>
	</header>
	<p>
		Martin Lambertsen<br>
		Email: <a href="mailto:martin.lambertsen@web.de">martin.lambertsen@web.de</a>
	</p>
</article>
<article class="inhalt_artikel">
	<header>
		<h3>Urheberrecht</h3>
	</header>
	<p>Sofern nicht explizit angegeben, unterliegen alle auf dieser Seite angebotenen Inhalte (Texte, Bilder und weitere Medieninhalte) dem Urheberrecht. Die Inhalte d&uuml;rfen ausschlie&szlig;lig mit vorheriger Genehmigung der Eigent&uuml;mer dieser Homepage in unver&auml;nderter Form und mit Angabe der Herkunft kopiert werden.
	</p>
</article>
<article class="inhalt_artikel">
	<header>
		<h3>Haftungshinweis</h3>
	</header>
	<p>Da wir f&uuml;r Links Dritter keine Haftung &uuml;bernehmen k&ouml;nnen, distanzieren wir uns hiermit aus rechtlichen Gr&uuml;nden generell und ausdr&uuml;cklich von verlinkten Seiten. F&uuml;r den Inhalt der verlinkten Seiten sind ausschlie&szlig;lich deren Betreiber verantwortlich.
	</p>
</article>
<article class="inhalt_artikel">
	<header>
		<h3>Hinweis zur Online-Streitbeilegung gem&auml;&szlig; Art. 14 Abs. 1 ODR-VO</h3>
	</header>
	<p>Die Europ&auml;ische Kommission stellt eine Plattform zur Online-Streitbeilegung (OS) bereit, die Sie unter <a href="https://ec.europa.eu/consumers/odr/" target="_blank">https://ec.europa.eu/consumers/odr/</a> finden.
	</p>
</article>
<article class="inhalt_artikel">
	<header>
		<h3>Datenschutzerkl&auml;rung</h3>
	</header>
	<p>
		Diese Datenschutzerkl&auml;rung kl&auml;rt Sie &uuml;ber die Art, den Umfang und Zweck der Verarbeitung von personenbezogenen Daten (nachfolgend kurz „Daten“) innerhalb unseres Onlineangebotes und der mit ihm verbundenen Webseiten, Funktionen und Inhalte sowie externen Onlinepr&auml;senzen, wie z.B. unser Social Media Profile auf. (nachfolgend gemeinsam bezeichnet als „Onlineangebot“). Im Hinblick auf die verwendeten Begrifflichkeiten, wie z.B. „Verarbeitung“ oder „Verantwortlicher“ verweisen wir auf die Definitionen im Art. 4 der Datenschutzgrundverordnung (DSGVO).<br>
	</p>
	<header>
		<h5>Verantwortlicher</h5>
	</header>
	<p>
		Arfst und Karin Lambertsen<br>
		S&uuml;&uuml;derwoi 5<br>
		25938 Borgsum<br>
		Email: lambertsen@borgsum.de
	</p>
	<header>
		<h5>Arten der verarbeiteten Daten:</h5>
	</header>
	<p>
		<ul>
			<li>Bestandsdaten (z.B., Namen, Adressen).</li>
			<li>Kontaktdaten (z.B., E-Mail, Telefonnummern).</li>
			<li>Inhaltsdaten (z.B., Texteingaben, Fotografien, Videos).</li>
			<li>Nutzungsdaten (z.B., besuchte Webseiten, Interesse an Inhalten, Zugriffszeiten).</li>
			<li>Meta-/Kommunikationsdaten (z.B., Ger&auml;te-Informationen, IP-Adressen).</li>
		</ul>
	</p>
	<header>
		<h5>Kategorien betroffener Personen</h5>
	</header>
	<p>
		Besucher und Nutzer des Onlineangebotes (Nachfolgend bezeichnen wir die betroffenen Personen zusammenfassend auch als „Nutzer“).<br>
	</p>
	<header>
		<h5>Zweck der Verarbeitung</h5>
	</header>
	<p>
		<ul>
			<li>Zurverf&uuml;gungstellung des Onlineangebotes, seiner Funktionen und  Inhalte.</li>
			<li>Beantwortung von Kontaktanfragen und Kommunikation mit Nutzern.</li>
			<li>Sicherheitsmaßnahmen.</li>
			<li>Reichweitenmessung/Marketing.</li>
		</ul>
	</p>
	<header>
		<h5>Verwendete Begrifflichkeiten </h5>
	</header>
	<p>
		„Personenbezogene Daten“ sind alle Informationen, die sich auf eine identifizierte oder identifizierbare nat&uuml;rliche Person (im Folgenden „betroffene Person“) beziehen; als identifizierbar wird eine nat&uuml;rliche Person angesehen, die direkt oder indirekt, insbesondere mittels Zuordnung zu einer Kennung wie einem Namen, zu einer Kennnummer, zu Standortdaten, zu einer Online-Kennung (z.B. Cookie) oder zu einem oder mehreren besonderen Merkmalen identifiziert werden kann, die Ausdruck der physischen, physiologischen, genetischen, psychischen, wirtschaftlichen, kulturellen oder sozialen Identit&auml;t dieser nat&uuml;rlichen Person sind.<br>
		<br>
		„Verarbeitung“ ist jeder mit oder ohne Hilfe automatisierter Verfahren ausgef&uuml;hrten Vorgang oder jede solche Vorgangsreihe im Zusammenhang mit personenbezogenen Daten. Der Begriff reicht weit und umfasst praktisch jeden Umgang mit Daten.<br>
		<br>
		Als „Verantwortlicher“ wird die nat&uuml;rliche oder juristische Person, Beh&ouml;rde, Einrichtung oder andere Stelle, die allein oder gemeinsam mit anderen &uuml;ber die Zwecke und Mittel der Verarbeitung von personenbezogenen Daten entscheidet, bezeichnet.
	</p>
	<header>
		<h5>Maßgebliche Rechtsgrundlagen</h5>
	</header>
	<p>
		Nach Maßgabe des Art. 13 DSGVO teilen wir Ihnen die Rechtsgrundlagen unserer Datenverarbeitungen mit. Sofern die Rechtsgrundlage in der Datenschutzerkl&auml;rung nicht genannt wird, gilt Folgendes: Die Rechtsgrundlage f&uuml;r die Einholung von Einwilligungen ist Art. 6 Abs. 1 lit. a und Art. 7 DSGVO, die Rechtsgrundlage f&uuml;r die Verarbeitung zur Erf&uuml;llung unserer Leistungen und Durchf&uuml;hrung vertraglicher Maßnahmen sowie Beantwortung von Anfragen ist Art. 6 Abs. 1 lit. b DSGVO, die Rechtsgrundlage f&uuml;r die Verarbeitung zur Erf&uuml;llung unserer rechtlichen Verpflichtungen ist Art. 6 Abs. 1 lit. c DSGVO, und die Rechtsgrundlage f&uuml;r die Verarbeitung zur Wahrung unserer berechtigten Interessen ist Art. 6 Abs. 1 lit. f DSGVO. F&uuml;r den Fall, dass lebenswichtige Interessen der betroffenen Person oder einer anderen nat&uuml;rlichen Person eine Verarbeitung personenbezogener Daten erforderlich machen, dient Art. 6 Abs. 1 lit. d DSGVO als Rechtsgrundlage.
	</p>
	<header>
		<h5>Zusammenarbeit mit Auftragsverarbeitern und Dritten</h5>
	</header>
	<p>
		Sofern wir im Rahmen unserer Verarbeitung Daten gegen&uuml;ber anderen Personen und Unternehmen (Auftragsverarbeitern oder Dritten) offenbaren, sie an diese &uuml;bermitteln oder ihnen sonst Zugriff auf die Daten gew&auml;hren, erfolgt dies nur auf Grundlage einer gesetzlichen Erlaubnis (z.B. wenn eine &Uuml;bermittlung der Daten an Dritte, wie an Zahlungsdienstleister, gem. Art. 6 Abs. 1 lit. b DSGVO zur Vertragserf&uuml;llung erforderlich ist), Sie eingewilligt haben, eine rechtliche Verpflichtung dies vorsieht oder auf Grundlage unserer berechtigten Interessen (z.B. beim Einsatz von Beauftragten, Webhostern, etc.). <br>
		<br>
		Sofern wir Dritte mit der Verarbeitung von Daten auf Grundlage eines sog. „Auftragsverarbeitungsvertrages“ beauftragen, geschieht dies auf Grundlage des Art. 28 DSGVO.
	</p>
	<header>
		<h5>&Uuml;bermittlungen in Drittl&auml;nder</h5>
	</header>
	<p>
		Sofern wir Daten in einem Drittland (d.h. außerhalb der Europ&auml;ischen Union (EU) oder des Europ&auml;ischen Wirtschaftsraums (EWR)) verarbeiten oder dies im Rahmen der Inanspruchnahme von Diensten Dritter oder Offenlegung, bzw. &Uuml;bermittlung von Daten an Dritte geschieht, erfolgt dies nur, wenn es zur Erf&uuml;llung unserer (vor)vertraglichen Pflichten, auf Grundlage Ihrer Einwilligung, aufgrund einer rechtlichen Verpflichtung oder auf Grundlage unserer berechtigten Interessen geschieht. Vorbehaltlich gesetzlicher oder vertraglicher Erlaubnisse, verarbeiten oder lassen wir die Daten in einem Drittland nur beim Vorliegen der besonderen Voraussetzungen der Art. 44 ff. DSGVO verarbeiten. D.h. die Verarbeitung erfolgt z.B. auf Grundlage besonderer Garantien, wie der offiziell anerkannten Feststellung eines der EU entsprechenden Datenschutzniveaus (z.B. f&uuml;r die USA durch das „Privacy Shield“) oder Beachtung offiziell anerkannter spezieller vertraglicher Verpflichtungen (so genannte „Standardvertragsklauseln“).
	</p>
	<header>
		<h5>Rechte der betroffenen Personen</h5>
	</header>
	<p>
		Sie haben das Recht, eine Best&auml;tigung dar&uuml;ber zu verlangen, ob betreffende Daten verarbeitet werden und auf Auskunft &uuml;ber diese Daten sowie auf weitere Informationen und Kopie der Daten entsprechend Art. 15 DSGVO.<br>
		<br>
		Sie haben entsprechend. Art. 16 DSGVO das Recht, die Vervollst&auml;ndigung der Sie betreffenden Daten oder die Berichtigung der Sie betreffenden unrichtigen Daten zu verlangen.<br>
		<br>
		Sie haben nach Maßgabe des Art. 17 DSGVO das Recht zu verlangen, dass betreffende Daten unverz&uuml;glich gel&ouml;scht werden, bzw. alternativ nach Maßgabe des Art. 18 DSGVO eine Einschr&auml;nkung der Verarbeitung der Daten zu verlangen.<br>
		<br>
		Sie haben das Recht zu verlangen, dass die Sie betreffenden Daten, die Sie uns bereitgestellt haben nach Maßgabe des Art. 20 DSGVO zu erhalten und deren &Uuml;bermittlung an andere Verantwortliche zu fordern. <br>
		<br>
		Sie haben ferner gem. Art. 77 DSGVO das Recht, eine Beschwerde bei der zust&auml;ndigen Aufsichtsbeh&ouml;rde einzureichen.
	</p>
	<header>
		<h5>Widerrufsrecht</h5>
	</header>
	<p>
		Sie haben das Recht, erteilte Einwilligungen gem. Art. 7 Abs. 3 DSGVO mit Wirkung f&uuml;r die Zukunft zu widerrufen.
	</p>
	<header>
		<h5>Widerspruchsrecht</h5>
	</header>
	<p>
		Sie k&ouml;nnen der k&uuml;nftigen Verarbeitung der Sie betreffenden Daten nach Maßgabe des Art. 21 DSGVO jederzeit widersprechen. Der Widerspruch kann insbesondere gegen die Verarbeitung f&uuml;r Zwecke der Direktwerbung erfolgen.
	</p>
	<header>
		<h5>Cookies und Widerspruchsrecht bei Direktwerbung</h5>
	</header>
	<p>
		Als „Cookies“ werden kleine Dateien bezeichnet, die auf Rechnern der Nutzer gespeichert werden. Innerhalb der Cookies k&ouml;nnen unterschiedliche Angaben gespeichert werden. Ein Cookie dient prim&auml;r dazu, die Angaben zu einem Nutzer (bzw. dem Ger&auml;t auf dem das Cookie gespeichert ist) w&auml;hrend oder auch nach seinem Besuch innerhalb eines Onlineangebotes zu speichern. Als tempor&auml;re Cookies, bzw. „Session-Cookies“ oder „transiente Cookies“, werden Cookies bezeichnet, die gel&ouml;scht werden, nachdem ein Nutzer ein Onlineangebot verl&auml;sst und seinen Browser schließt. In einem solchen Cookie kann z.B. der Inhalt eines Warenkorbs in einem Onlineshop oder ein Login-Staus gespeichert werden. Als „permanent“ oder „persistent“ werden Cookies bezeichnet, die auch nach dem Schließen des Browsers gespeichert bleiben. So kann z.B. der Login-Status gespeichert werden, wenn die Nutzer diese nach mehreren Tagen aufsuchen. Ebenso k&ouml;nnen in einem solchen Cookie die Interessen der Nutzer gespeichert werden, die f&uuml;r Reichweitenmessung oder Marketingzwecke verwendet werden. Als „Third-Party-Cookie“ werden Cookies bezeichnet, die von anderen Anbietern als dem Verantwortlichen, der das Onlineangebot betreibt, angeboten werden (andernfalls, wenn es nur dessen Cookies sind spricht man von „First-Party Cookies“).<br>
		<br>
		Wir k&ouml;nnen tempor&auml;re und permanente Cookies einsetzen und kl&auml;ren hier&uuml;ber im Rahmen unserer Datenschutzerkl&auml;rung auf.<br>
		<br>
		Falls die Nutzer nicht m&ouml;chten, dass Cookies auf ihrem Rechner gespeichert werden, werden sie gebeten die entsprechende Option in den Systemeinstellungen ihres Browsers zu deaktivieren. Gespeicherte Cookies k&ouml;nnen in den Systemeinstellungen des Browsers gel&ouml;scht werden. Der Ausschluss von Cookies kann zu Funktionseinschr&auml;nkungen dieses Onlineangebotes f&uuml;hren.<br>
		<br>
		Ein genereller Widerspruch gegen den Einsatz der zu Zwecken des Onlinemarketing eingesetzten Cookies kann bei einer Vielzahl der Dienste, vor allem im Fall des Trackings, &uuml;ber die US-amerikanische Seite <a href="http://www.aboutads.info/choices/" target="_blank">http://www.aboutads.info/choices/</a> oder die EU-Seite <a href="http://www.youronlinechoices.com/" target="_blank">http://www.youronlinechoices.com/</a> erkl&auml;rt werden. Des Weiteren kann die Speicherung von Cookies mittels deren Abschaltung in den Einstellungen des Browsers erreicht werden. Bitte beachten Sie, dass dann gegebenenfalls nicht alle Funktionen dieses Onlineangebotes genutzt werden k&ouml;nnen.
	</p>
	<header>
		<h5>L&ouml;schung von Daten</h5>
	</header>
	<p>
		Die von uns verarbeiteten Daten werden nach Maßgabe der Art. 17 und 18 DSGVO gel&ouml;scht oder in ihrer Verarbeitung eingeschr&auml;nkt. Sofern nicht im Rahmen dieser Datenschutzerkl&auml;rung ausdr&uuml;cklich angegeben, werden die bei uns gespeicherten Daten gel&ouml;scht, sobald sie f&uuml;r ihre Zweckbestimmung nicht mehr erforderlich sind und der L&ouml;schung keine gesetzlichen Aufbewahrungspflichten entgegenstehen. Sofern die Daten nicht gel&ouml;scht werden, weil sie f&uuml;r andere und gesetzlich zul&auml;ssige Zwecke erforderlich sind, wird deren Verarbeitung eingeschr&auml;nkt. D.h. die Daten werden gesperrt und nicht f&uuml;r andere Zwecke verarbeitet. Das gilt z.B. f&uuml;r Daten, die aus handels- oder steuerrechtlichen Gr&uuml;nden aufbewahrt werden m&uuml;ssen.<br>
		<br>
		Nach gesetzlichen Vorgaben in Deutschland erfolgt die Aufbewahrung insbesondere f&uuml;r 6 Jahre gem&auml;ß § 257 Abs. 1 HGB (Handelsb&uuml;cher, Inventare, Er&ouml;ffnungsbilanzen, Jahresabschl&uuml;sse, Handelsbriefe, Buchungsbelege, etc.) sowie f&uuml;r 10 Jahre gem&auml;ß § 147 Abs. 1 AO (B&uuml;cher, Aufzeichnungen, Lageberichte, Buchungsbelege, Handels- und Gesch&auml;ftsbriefe, F&uuml;r Besteuerung relevante Unterlagen, etc.).
	</p>
	<header>
		<h5>Gesch&auml;ftsbezogene Verarbeitung</h5>
	</header>
	<p>
		Zus&auml;tzlich verarbeiten wir
	</p>
		<ul>
			<li>Vertragsdaten (z.B., Vertragsgegenstand, Laufzeit, Kundenkategorie).</li>
			<li>Zahlungsdaten (z.B., Bankverbindung, Zahlungshistorie)</li>
		</ul>
	<p>
		von unseren Kunden, Interessenten und Gesch&auml;ftspartner zwecks Erbringung vertraglicher Leistungen, Service und Kundenpflege, Marketing, Werbung und Marktforschung.
	</p>
	<header>
		<h5>Hosting</h5>
	</header>
	<p>
		Die von uns in Anspruch genommenen Hosting-Leistungen dienen der Zurverf&uuml;gungstellung der folgenden Leistungen: Infrastruktur- und Plattformdienstleistungen, Rechenkapazit&auml;t, Speicherplatz und Datenbankdienste, Sicherheitsleistungen sowie technische Wartungsleistungen, die wir zum Zwecke des Betriebs dieses Onlineangebotes einsetzen.<br>
		<br>
		Hierbei verarbeiten wir, bzw. unser Hostinganbieter Bestandsdaten, Kontaktdaten, Inhaltsdaten, Vertragsdaten, Nutzungsdaten, Meta- und Kommunikationsdaten von Kunden, Interessenten und Besuchern dieses Onlineangebotes auf Grundlage unserer berechtigten Interessen an einer effizienten und sicheren Zurverf&uuml;gungstellung dieses Onlineangebotes gem. Art. 6 Abs. 1 lit. f DSGVO i.V.m. Art. 28 DSGVO (Abschluss Auftragsverarbeitungsvertrag).
	</p>
	<header>
		<h5>Erhebung von Zugriffsdaten und Logfiles</h5>
	</header>
	<p>
		Wir, bzw. unser Hostinganbieter, erhebt auf Grundlage unserer berechtigten Interessen im Sinne des Art. 6 Abs. 1 lit. f. DSGVO Daten &uuml;ber jeden Zugriff auf den Server, auf dem sich dieser Dienst befindet (sogenannte Serverlogfiles). Zu den Zugriffsdaten geh&ouml;ren Name der abgerufenen Webseite, Datei, Datum und Uhrzeit des Abrufs, &uuml;bertragene Datenmenge, Meldung &uuml;ber erfolgreichen Abruf, Browsertyp nebst Version, das Betriebssystem des Nutzers, Referrer URL (die zuvor besuchte Seite), IP-Adresse und der anfragende Provider.<br>
		<br>
		Logfile-Informationen werden aus Sicherheitsgr&uuml;nden (z.B. zur Aufkl&auml;rung von Missbrauchs- oder Betrugshandlungen) f&uuml;r die Dauer von maximal 7 Tagen gespeichert und danach gel&ouml;scht. Daten, deren weitere Aufbewahrung zu Beweiszwecken erforderlich ist, sind bis zur endg&uuml;ltigen Kl&auml;rung des jeweiligen Vorfalls von der L&ouml;schung ausgenommen.
	</p>
	<header>
		<h5>Erbringung vertraglicher Leistungen</h5>
	</header>
	<p>
		Wir verarbeiten Bestandsdaten (z.B., Namen und Adressen sowie Kontaktdaten von Nutzern), Vertragsdaten (z.B., in Anspruch genommene Leistungen, Namen von Kontaktpersonen, Zahlungsinformationen) zwecks Erf&uuml;llung unserer vertraglichen Verpflichtungen und Serviceleistungen gem. Art. 6 Abs. 1 lit b. DSGVO. Die in Onlineformularen als verpflichtend gekennzeichneten Eingaben, sind f&uuml;r den Vertragsschluss erforderlich.<br>
		<br>
		Im Rahmen der Inanspruchnahme unserer Onlinedienste, speichern wir die IP-Adresse und den Zeitpunkt der jeweiligen Nutzerhandlung. Die Speicherung erfolgt auf Grundlage unserer berechtigten Interessen, als auch der Nutzer an Schutz vor Missbrauch und sonstiger unbefugter Nutzung. Eine Weitergabe dieser Daten an Dritte erfolgt grunds&auml;tzlich nicht, außer sie ist zur Verfolgung unserer Anspr&uuml;che erforderlich oder es besteht hierzu eine gesetzliche Verpflichtung gem. Art. 6 Abs. 1 lit. c DSGVO.<br>
		<br>
		Wir verarbeiten Nutzungsdaten (z.B., die besuchten Webseiten unseres Onlineangebotes, Interesse an unseren Produkten) und Inhaltsdaten (z.B., Eingaben im Kontaktformular oder Nutzerprofil) f&uuml;r Werbezwecke in einem Nutzerprofil, um den Nutzer z.B. Produkthinweise ausgehend von ihren bisher in Anspruch genommenen Leistungen einzublenden.<br>
		<br>
		Die L&ouml;schung der Daten erfolgt nach Ablauf gesetzlicher Gew&auml;hrleistungs- und vergleichbarer Pflichten, die Erforderlichkeit der Aufbewahrung der Daten wird alle drei Jahre &uuml;berpr&uuml;ft; im Fall der gesetzlichen Archivierungspflichten erfolgt die L&ouml;schung nach deren Ablauf. Angaben im etwaigen Kundenkonto verbleiben bis zu dessen L&ouml;schung.
	</p>
	<header>
		<h5>Administration, Finanzbuchhaltung, B&uuml;roorganisation, Kontaktverwaltung</h5>
	</header>
	<p>
		Wir verarbeiten Daten im Rahmen von Verwaltungsaufgaben sowie Organisation unseres Betriebs, Finanzbuchhaltung und Befolgung der gesetzlichen Pflichten, wie z.B. der Archivierung. Herbei verarbeiten wir dieselben Daten, die wir im Rahmen der Erbringung unserer vertraglichen Leistungen verarbeiten. Die Verarbeitungsgrundlagen sind Art. 6 Abs. 1 lit. c. DSGVO, Art. 6 Abs. 1 lit. f. DSGVO. Von der Verarbeitung sind Kunden, Interessenten, Gesch&auml;ftspartner und Websitebesucher betroffen. Der Zweck und unser Interesse an der Verarbeitung liegt in der Administration, Finanzbuchhaltung, B&uuml;roorganisation, Archivierung von Daten, also Aufgaben die der Aufrechterhaltung unserer Gesch&auml;ftst&auml;tigkeiten, Wahrnehmung unserer Aufgaben und Erbringung unserer Leistungen dienen. Die L&ouml;schung der Daten im Hinblick auf vertragliche Leistungen und die vertragliche Kommunikation entspricht den, bei diesen Verarbeitungst&auml;tigkeiten genannten Angaben.<br>
		<br>
		Wir offenbaren oder &uuml;bermitteln hierbei Daten an die Finanzverwaltung, Berater, wie z.B., Steuerberater oder Wirtschaftspr&uuml;fer sowie weitere Geb&uuml;hrenstellen und Zahlungsdienstleister.<br>
		<br>
		Ferner speichern wir auf Grundlage unserer betriebswirtschaftlichen Interessen Angaben zu Lieferanten, Veranstaltern und sonstigen Gesch&auml;ftspartnern, z.B. zwecks sp&auml;terer Kontaktaufnahme. Diese mehrheitlich unternehmensbezogenen Daten, speichern wir grunds&auml;tzlich dauerhaft.<br>
	</p>
	<header>
		<h5>Kontaktaufnahme</h5>
	</header>
	<p>
		Bei der Kontaktaufnahme mit uns (z.B. per Kontaktformular, E-Mail, Telefon oder via sozialer Medien) werden die Angaben des Nutzers zur Bearbeitung der Kontaktanfrage und deren Abwicklung gem. Art. 6 Abs. 1 lit. b) DSGVO verarbeitet. Die Angaben der Nutzer k&ouml;nnen in einem Customer-Relationship-Management System ("CRM System") oder vergleichbarer Anfragenorganisation gespeichert werden.<br>
		<br>
		Wir l&ouml;schen die Anfragen, sofern diese nicht mehr erforderlich sind. Wir &uuml;berpr&uuml;fen die Erforderlichkeit alle zwei Jahre; Ferner gelten die gesetzlichen Archivierungspflichten.
	</p>
	<header>
		<h5>Einbindung von Diensten und Inhalten Dritter</h5>
	</header>
	<p>
		Wir setzen innerhalb unseres Onlineangebotes auf Grundlage unserer berechtigten Interessen (d.h. Interesse an der Analyse, Optimierung und wirtschaftlichem Betrieb unseres Onlineangebotes im Sinne des Art. 6 Abs. 1 lit. f. DSGVO) Inhalts- oder Serviceangebote von Drittanbietern ein, um deren Inhalte und Services, wie z.B. Videos oder Schriftarten einzubinden (nachfolgend einheitlich bezeichnet als “Inhalte”).<br>
		<br>
		Dies setzt immer voraus, dass die Drittanbieter dieser Inhalte, die IP-Adresse der Nutzer wahrnehmen, da sie ohne die IP-Adresse die Inhalte nicht an deren Browser senden k&ouml;nnten. Die IP-Adresse ist damit f&uuml;r die Darstellung dieser Inhalte erforderlich. Wir bem&uuml;hen uns nur solche Inhalte zu verwenden, deren jeweilige Anbieter die IP-Adresse lediglich zur Auslieferung der Inhalte verwenden. Drittanbieter k&ouml;nnen ferner so genannte Pixel-Tags (unsichtbare Grafiken, auch als "Web Beacons" bezeichnet) f&uuml;r statistische oder Marketingzwecke verwenden. Durch die "Pixel-Tags" k&ouml;nnen Informationen, wie der Besucherverkehr auf den Seiten dieser Website ausgewertet werden. Die pseudonymen Informationen k&ouml;nnen ferner in Cookies auf dem Ger&auml;t der Nutzer gespeichert werden und unter anderem technische Informationen zum Browser und Betriebssystem, verweisende Webseiten, Besuchszeit sowie weitere Angaben zur Nutzung unseres Onlineangebotes enthalten, als auch mit solchen Informationen aus anderen Quellen verbunden werden.
	</p>
	<header>
		<h5>Youtube</h5>
	</header>
	<p>
		Wir binden die Videos der Plattform “YouTube” des Anbieters Google LLC, 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA, ein. Datenschutzerkl&auml;rung: <a href="https://www.google.com/policies/privacy/" target="_blank">https://www.google.com/policies/privacy/</a>, Opt-Out: <a href="https://adssettings.google.com/authenticated" target="_blank">https://adssettings.google.com/authenticated</a>.
	</p>
	<header>
		<h5>Google Fonts</h5>
	</header>
	<p>
		Wir binden die Schriftarten ("Google Fonts") des Anbieters Google LLC, 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA, ein. Datenschutzerkl&auml;rung: <a href="https://www.google.com/policies/privacy/" target="_blank">https://www.google.com/policies/privacy/</a>, Opt-Out: <a href="https://adssettings.google.com/authenticated" target="_blank">https://adssettings.google.com/authenticated</a>.
	</p>
	<p>
		Vom Websiteinhaber angepasst:<br>
		<a href="https://datenschutz-generator.de" rel="nofollow" target="_blank">Erstellt mit Datenschutz-Generator.de von RA Dr. Thomas Schwenke</a>
	</p>
</article>
