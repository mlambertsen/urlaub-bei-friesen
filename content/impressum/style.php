<?php
	header("Content-type: text/css");
?>

.impressum_einfuehrung
{
	padding: 0;
	margin-bottom: 25px; /* Ueberschreibt den ersten margin-top, Stichwort: Collapsing margins */
	margin-bottom: 1.5625rem;
}


@media (min-width: 540px)
{
	.impressum_links
	{
		margin-right: 1.5%;
		width: 42.5%;
		float: left;
	}

	.impressum_rechts
	{
		margin-left: 1.5%;
		width: 42.5%;
		float: left;
	}
}