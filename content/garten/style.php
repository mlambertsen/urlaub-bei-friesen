<?php
	header("Content-type: text/css");
?>

.inhalt_spielplatz_links
{
	width: 30%;
	float: left;
	padding-right: 2%;
}

.inhalt_spielplatz_rechts
{
	width: 30%;
	float: right;
	padding-left: 2%;
}

.inhalt_liegestuehle_links
{
	width: 60%;
	float: left;
	padding-right: 2%;
}

.inhalt_garten_info_rechts
{
	width: 60%;
	float: right;
	padding-left: 2%;
}