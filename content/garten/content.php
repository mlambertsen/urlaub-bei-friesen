<article class="inhalt_artikel">
	<header>
		<h3>Das Entspannungsidyll</h3>
	</header>
	<div class="inhalt_liegestuehle_links">
		<img style="width: 100%" src="garten/images/Haus_Lambertsen_Liegestuehle.jpg" alt="Haus Lambertsen - Liegest&uuml;hle im Garten">
	</div>
	<p>Im sonnigen Garten des knapp 1000 m<sup>2</sup> gro&szlig;en Grundst&uuml;ckes finden Sie &uuml;berall ruhige Sitzecken mit Gartenm&ouml;beln und Liegest&uuml;hlen.
	</p>
	<p>Durch die ruhige Lage in einer Sackgasse, l&auml;sst es sich hier zu jeder Zeit entspannen, Sonne tanken, kurzum: Den Tag genie&szlig;en.
	</p>
</article>

<article class="inhalt_artikel">
	<header>
		<h3>Der Hauseigene Spielplatz</h3>
	</header>
	<div class="inhalt_spielplatz_links">
		<img style="width: 100%" src="garten/images/Haus_Lambertsen_Spielplatz.jpg" alt="Haus Lambertsen Spielplatz">
	</div>
	<div class="inhalt_spielplatz_rechts">
		<img style="width: 100%" src="garten/images/Haus_Lambertsen_Tischtennis.jpg" alt="Haus Lambertsen Tischtennisplatte">
	</div>
	<p>Kinder k&ouml;nnen auf dem hauseigenen Spielplatz mit Schaukeln, Sandkiste und Gartenh&auml;uschen voller Spielsachen spielen.</p>
	<p>F&uuml;r Kinder jeden Alters steht eine Tischtennisplatte bereit.</p>
</article>

<article class="inhalt_artikel">
	<header>
		<h3>Weitere Informationen</h3>
	</header>
	<div class="inhalt_garten_info_rechts">
		<img style="width: 100%" src="garten/images/Haus_Lambertsen_Blumen.jpg" alt="Blumen im Garten von Haus Lambertsen">
	</div>
	<p>Wir sind beide leidenschaftliche G&auml;rtner. So k&ouml;nnen Sie zu (fast) jeder Jahreszeit direkt die Sch&ouml;nheit der Natur bewundern.</p>
	<p>Auf unserem Grundst&uuml;ck finden Sie au&szlig;erdem</p>
	<ul class="ul_checkmark">
		<li>einen Parkplatz für Ihr Auto und eine Garage für Ihre Fahrräder und Strandutensilien.</li>
		<li>einen Grill f&uuml;r sch&ouml;ne Sommerabende.</li>
	</ul>
</article>