<?php
	header("Content-type: text/css");
?>

.inhalt_haus_links
{
	width: 32%;
	float: left;
	padding-right: 2%;
	padding-bottom: 15px;
	padding-bottom: 0.9375rem;
}

.inhalt_haus_rechts
{
	width: 32%;
	float: right;
	padding-left: 2%;
	padding-bottom: 15px;
	padding-bottom: 0.9375rem;
}

.inhalt_kueche_links
{
	width: 60%;
	float: left;
	padding-right: 2%;
}

.inhalt_wohnzimmer_rechts
{
	width: 60%;
	float: right;
	padding-left: 2%;
}

.inhalt_schlafzimmer_links
{
	width: 34%;
	float: left;
	padding-right: 2%;
}

.inhalt_schlafzimmer_rechts
{
	width: 34%;
	float: right;
	padding-left: 2%;
	padding-bottom: 15px;
	padding-bottom: 0.9375rem;
}

.inhalt_grundriss_rechts
{
	width: 50%;
	float: right;
}