<article class="inhalt_artikel">
	<header>
		<h3>Das Haus und die Lage</h3>
	</header>
	<div class="inhalt_haus_links">
		<img style="width: 100%" src="wohnungen/images/Haus_Lambertsen.jpg" alt="Haus Lambertsen">
	</div>
	<div class="inhalt_haus_rechts">
		<img style="width: 100%" src="wohnungen/images/Haus_Lambertsen_Lage.jpg" alt="Haus Lambertsen Lage auf der Insel F&ouml;hr">
	</div>
	<p>Hier wohnen Sie, sehr ruhig in einer Sackgasse nur wenige Minuten vom Strand entfernt und zentral auf der Insel.</p>
	<p>Diese zentrale Lage eignet sich ideal zum Fahrradfahren und Wandern über die Insel.</p>
	<p>Einkaufsm&ouml;glichkeiten, Gastronomie, Hofladen, Eiscaf&eacute;, Fahrradverleih, Bushaltestelle, Bowlingbahn und Reitm&ouml;glichkeit befinden sich in unmittelbarer N&auml;he.</p>
</article>

<article class="inhalt_artikel">
	<header>
		<h3>Die ger&auml;umige Wohnk&uuml;che</h3>
	</header>
	<div class="inhalt_kueche_links">
		<img style="width: 100%" src="wohnungen/images/Haus_Lambertsen_Kueche.jpg" alt="Küche im Haus Lambertsen">
	</div>
	<p>Die Wohnk&uuml;che ist komplett eingerichtet. Vom Geschirrsp&uuml;ler, 4-Plattenherd, Backofen, K&uuml;hlschrank mit Gefrierfach, Kaffeemaschine, Wasserkocher, Toaster...bis hin zum Dosen&ouml;ffner ist alles vorhanden.</p>
	<p>In der gem&uuml;tlichen Essecke mit Radio und Kinderhochstuhl werden Sie sicher nicht nur Ihren Hunger stillen, sondern auch zum Spielen von Gesellschaftsspielen, Malen, Basteln usw. verweilen.</p>
</article>

<article class="inhalt_artikel">
	<header>
		<h3>Das gem&uuml;tliche Wohnzimmer</h3>
	</header>
	<div class="inhalt_wohnzimmer_rechts">
		<img style="width: 100%" src="wohnungen/images/Haus_Lambertsen_Wohnzimmer.jpg" alt="Wohnzimmer im Haus Lambertsen">
	</div>
	<p>
		Die gem&uuml;tliche Sofaecke im Wohnzimmer l&auml;dt zum Entspannen und Lesen ein. Auch ein moderner Full-HD Fernseher und ein DVD-Player stehen zur Verf&uuml;gung.
	</p>
	<p>
		Wie in der gesamten Wohnung besteht die M&ouml;glichkeit über W-LAN kostenlos im Internet zu surfen.
	</p>
</article>

<article class="inhalt_artikel">
	<header>
		<h3>Die Betten</h3>
	</header>
	<div class="inhalt_schlafzimmer_links">
		<img style="width: 100%" src="wohnungen/images/Haus_Lambertsen_Betten.jpg" alt="Die Betten im Haus Lambertsen">
	</div>
	<div class="inhalt_schlafzimmer_rechts">
		<img style="width: 100%" src="wohnungen/images/Haus_Lambertsen_Schrankehebett.jpg" alt="Das Schrankehebett im Haus Lambertsen">
	</div>
	<p>Im Schlafzimmer werden Sie tief und erholsam schlafen. Die Betten können wahlweise als Ehebett oder als Einzelbetten gestellt werden. Für die ganz kleinen Gäste halten wir ein Kinderbett bereit.</p>
	<p>Für gr&ouml;&szlig;ere Familien l&auml;sst sich der Wohnraum dank der Schrankwand mit Kleiderschrank und Schrankehebett schnell in ein behagliches Schlafzimmer verwandeln. So k&ouml;nnen Sie sich vom Bett aus DVDs anschauen bzw. fernsehen und sich vom Tag ausruhen.</p>
</article>

<article class="inhalt_artikel">
	<header>
		<h3>Grundriss und weitere Daten</h3>
	</header>
	<div class="inhalt_grundriss_rechts">
		<img style="width: 100%" src="wohnungen/images/Haus_Lambertsen_Grundriss.png" alt="Grundriss vom Haus Lambertsen">
	</div>
	<p>
		Die beiden hell und freundlich eingerichteten Wohnungen sind ca. 60 m<sup>2</sup> gro&szlig; und für Allergiker geeignet.
	</p>
	<ul class="ul_checkmark">
		<li>Bettw&auml;sche, Handt&uuml;cher und Internetanschlu&szlig; über W-LAN sind im Mietpreis enthalten.</li>
		<li>Waschmaschine und Trockner können gegen eine geringe Geb&uuml;hr (4&euro;) benutzt werden.</li>
		<li>Kinderbett und Hochstuhl stellen wir Ihnen kostenlos zur Verf&uuml;gung.</li>
	</ul>
</article>