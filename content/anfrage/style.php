<?php
	header("Content-type: text/css");

	require_once("../common/style/colors.php");

?>

#inhalt_anfrage_links
{
	width: 58%;
	float:left;
	padding-right: 1%;
	border-right: dotted;
	border-right-width: 1px;
	border-right-width: 0.0625rem;
	border-right-color: <?php echo $main_color_dark?>;
}

#inhalt_anfrage_rechts
{
	width: 35%;
	float:left;
	padding-left: 5%;
}

#inhalt_anfrage_bild
{
	width: 94%;
	padding: 1em 3%;
	padding: 1rem 3%;
}


/* Kalender */
#kalender_tabelle_aussen
{
	text-align: center;
	width: 100%;
}

#kalender_tabelle_aussen th
{
	color: <?php echo $text_color?>;
	font-size: 15px;
	font-size: 0.9375rem;
}

#kalender_tabelle_aussen td
{
	color: <?php echo $main_color_light?>;
	width: 14%;
	border-width: 3px;
	border-width: 0.1875rem;
	border-style: outset;
}

.kalender_tabelle_innen
{
	width: 90%;
	margin: 0 auto;
}

.calendar_occupancy_0
{
	background-color: <?php echo $text_color?>;
}

.calendar_occupancy_1
{
	background-color: <?php echo $main_color_dark?>;
}

.calendar_occupancy_2
{
	background-color: <?php echo $second_color_dark?>;
}

.calendar_arrival_0_to_1
{
	background: url(images/calendar/arrival_0_to_1.png) no-repeat center center;
	background-size: cover;
	-moz-background-size: cover;
	-webkit-background-size: cover;
	-o-background-size: cover;
}

.calendar_arrival_0_to_2
{
	background: url(images/calendar/arrival_0_to_2.png) no-repeat center center;
	background-size: cover;
	-moz-background-size: cover;
	-webkit-background-size: cover;
	-o-background-size: cover;
}

.calendar_arrival_1_to_2
{
	background: url(images/calendar/arrival_1_to_2.png) no-repeat center center;
	background-size: cover;
	-moz-background-size: cover;
	-webkit-background-size: cover;
	-o-background-size: cover;
}

.calendar_departure_2_to_1
{
	background: url(images/calendar/departure_2_to_1.png) no-repeat center center;
	background-size: cover;
	-moz-background-size: cover;
	-webkit-background-size: cover;
	-o-background-size: cover;
}

.calendar_departure_2_to_0
{
	background: url(images/calendar/departure_2_to_0.png) no-repeat center center;
	background-size: cover;
	-moz-background-size: cover;
	-webkit-background-size: cover;
	-o-background-size: cover;
}

.calendar_departure_1_to_0
{
	background: url(images/calendar/departure_1_to_0.png) no-repeat center center;
	background-size: cover;
	-moz-background-size: cover;
	-webkit-background-size: cover;
	-o-background-size: cover;
}

#kalender_legende
{
	align: center;
}

#kalender_legende table
{
	margin: 0 auto;
}

#kalender_legende th
{
	text-align: right;
	padding: 0 5px;
	padding: 0 0.3125rem;
}

#kalender_legende td
{
	text-align: left;
	padding: 0 5px;
	padding: 0 0.3125rem;
}

#kalender_legende img
{
	height: 15px;
	height: 0.9375rem;
}

#button_kalender_fruher
{
	float: left;
}

#button_kalender_spater
{
	float: right;
}


/* Einstellungen zum Formular */
#hinweis_oben
{
	color: <?php echo $main_color_light?>;
	background-color: <?php echo $alert_color?>;
	-moz-border-radius: 8px;
	-webkit-border-radius: 8px;
	border-radius: 8px;
	-moz-border-radius: 0.5rem;
	-webkit-border-radius: 0.5rem;
	border-radius: 0.5rem;
	padding: 5px 2%;
	padding: 0.3125rem 2%;
}

legend
{
	color: <?php echo $main_color_dark?>;
	font-family: 'Lora', serif;
	font-size: 15px;
	font-size: 0.9375rem;
	font-weight: bold;
	margin-left: 1em;
	margin-left: 1rem;
}

fieldset
{
	border: 1px <?php echo $main_color_dark?> solid;
	margin: 0.8em 0 0 0;
	margin: 0.8rem 0 0 0;
	padding: 0;
	-moz-border-radius: 8px;
	-webkit-border-radius: 8px;
	border-radius: 8px;
	-moz-border-radius: 0.5rem;
	-webkit-border-radius: 0.5rem;
	border-radius: 0.5rem;
}

.formular_list
{
	list-style: none;
	margin: 0;
	padding: 1em 3% 0 3%;
	padding: 1rem 3% 0 3%;
}

.formular_list li
{
	float: left;
	clear: left;
	width: 100%;
	padding-bottom: 1em;
	padding-bottom: 1rem;
}

label, .formular_list input:not(.button), .formular_list select
{
	color: <?php echo $text_color?>;
	font-family: 'Lora', serif;
	-moz-hyphens: none;
	-o-hyphens: none;
	-webkit-hyphens: none;
	-ms-hyphens: none;
	hyphens: none;
	font-size: 15px;
	font-size: 0.9375rem;
	line-height: 24px;
	line-height: 1.5rem;
	height: 24px;
	height: 1.5rem;
}

input[type=date], input[type=text], input[type=email], input[type=tel], textarea, select
{
	-webkit-appearance: none;
	-moz-appearance: none;
	appearance: none;
	resize: none;
	outline: none;
	margin: 0;
	border: none;
	background-color: #fff;
	padding: 1px 4px;
	border-radius: 5px;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	box-shadow: 0px 0px 2px 1px <?php echo $main_color_dark?> inset;
	-moz-box-shadow: 0px 0px 2px 1px <?php echo $main_color_dark?> inset;
	-webkit-box-shadow: 0px 0px 2px 1px <?php echo $main_color_dark?> inset;
}

select
{
	background: url('../anfrage/images/select.svg') no-repeat #fff;
	background-size: 0.7rem;
	background-position: 80% center;
	width: 40px;
	width: 2.5rem;
}

select::-ms-expand
{
	display: none;
}

select option
{
	background-color: #fff;
	text-align: left;
	padding-left: 8px;
}

input[type="checkbox"]
{
	display: inline-block;
	margin: 2px 0 0;
	position: absolute;
	opacity: 0;
	z-index: 0;
}

input[type="checkbox"] + label
{
	float: none;
	position: relative;
	z-index: 1;
}

input[type="checkbox"] + label span
{
	display: inline-block;
	width: 19.2px;
	width: 1.2rem;
	height: 19.2px;
	height: 1.2rem;
	margin: 0 8px 0 0;
	margin: 0 0.5rem 0 0;
	vertical-align: middle;
	background: url('../anfrage/images/checkbox_inaktiv.svg') no-repeat;
	background-size: 100% 100%;
	cursor: pointer;
}

input[type="checkbox"]:checked + label span
{
	background: url('../anfrage/images/checkbox_aktiv.svg') no-repeat;
	background-size: 100% 100%;
}

input[type=date]:focus, input[type=text]:focus, input[type=email]:focus, input[type=tel]:focus, textarea:focus, select:focus
{
	box-shadow: 0px 0px 2px 1px <?php echo $text_color?> inset;
	-moz-box-shadow: 0px 0px 2px 1px <?php echo $text_color?> inset;
	-webkit-box-shadow: 0px 0px 2px 1px <?php echo $text_color?> inset;
}

input[type="submit"]
{
	margin-right: 16px;
	margin-right: 1rem;
}

label
{
	float: left;
	text-align: right;
	margin-right: 2%;
	height: auto;
}

fieldset:first-of-type label
{
	width: 50%;
}

fieldset:first-of-type input
{
	width: 8em;
	width: 8rem;
}

fieldset:nth-of-type(2) label
{
	width: 20%;
}

fieldset:nth-of-type(2) input
{
	width: 60%;
}

#gast_nachricht_label
{
	width: 40%;
	text-align: left;
}

textarea
{
	height: 10em;
	height: 10rem;
	width: 50%;
	width: calc(58% - 8px);
	color: <?php echo $text_color?>;
	font-family: 'Lora', serif;
	-moz-hyphens: none;
	-o-hyphens: none;
	-webkit-hyphens: none;
	-ms-hyphens: none;
	hyphens: none;
	font-size: 15px;
	font-size: 0.9375rem;
	line-height: 24px;
	line-height: 1.5rem;
}

p.align_right
{
	float: right;
}


.anfrage_email_bestaetigung
{
	/* Bots */
	display: none;
}







/* Kalender */
@media (max-width: 410px)
{
	#kalender_legende
	{
		clear: both;
		padding-top: 8px;
		padding-top: 0.5rem;
	}
}


/* Formular */
@media (max-width: 434px)
{
	fieldset:first-of-type label
	{
		width: 58%;
	}

	fieldset:nth-of-type(2) input
	{
		width: 70%;
	}
}

@media (max-width: 379px)
{
	fieldset:first-of-type label
	{
		width: 100%;
		margin: 0;
		text-align: left;
	}
}

@media (max-width: 310px)
{
	fieldset:nth-of-type(2) label
	{
		width: 100%;
		margin: 0;
		text-align: left;
	}

	fieldset:nth-of-type(2) input
	{
		width: 95%;
	}

	#gast_nachricht_label
	{
		width: 100%;
	}

	textarea
	{
		width: 90%;
		width: calc(100% - 8px);
	}
}

@media (max-width: 214px)
{
	p.align_right
	{
		text-align: left;
	}
}


/* Adresse und Bild */
@media (max-width: 739px)
{
	#inhalt_anfrage_links
	{
		width: 100%;
		padding-right: 0;
		border-right: none;
		border-bottom: dotted;
		border-bottom-width: 1px;
		border-bottom-width: 0.0625rem;
		border-bottom-color: <?php echo $main_color_dark?>;
	}

	#inhalt_anfrage_rechts
	{
		width: 100%;
		padding-left: 0;
	}

	#inhalt_anfrage_bild
	{
		width: 30%;
		float: right;
		padding-right: 15%;
	}
}

@media (max-width: 549px)
{
	#inhalt_anfrage_bild
	{
		width: 40%;
		padding-right: 0;
	}
}

@media (max-width: 459px)
{
	#inhalt_anfrage_bild
	{
		width: 30%;
		padding-top: 3em;
		padding-top: 3rem;
	}
}

@media (max-width: 389px)
{
	#inhalt_anfrage_bild
	{
		padding: 1em 25% 0 25%;
		padding: 1rem 25% 0 25%;
		width: 50%;
		float: none;
	}
}
