const BOOKING_PERIODS_LEFT = extractBookingPeriodsFromRawInformations(
    raw_booking_informations_left_from_php);
const BOOKING_PERIODS_RIGHT = extractBookingPeriodsFromRawInformations(
    raw_booking_informations_right_from_php);
var month_offset = 0;

$('#belegungskalender').ready(function() {
  loadCalendar();
});

$('#button_kalender_fruher').click(function() {
  decrementOffsetAndLoadCalendar();
});

$('#button_kalender_spater').click(function() {
  incrementOffsetAndLoadCalendar();
});

addResizeEvent(loadCalendar);


function incrementOffsetAndLoadCalendar() {
  document.activeElement.blur();
  month_offset += 1;
  loadCalendar()

  return false;
}

function decrementOffsetAndLoadCalendar() {
  document.activeElement.blur();
  month_offset -= 1;
  loadCalendar();

  return false;
}

function hideElementWithId(element_id) {
  if (!document.getElementById(element_id).classList.contains('hide')) {
    document.getElementById(element_id).classList.add('hide');
  }
}

function unveilElementWithId(element_id) {
  if (document.getElementById(element_id).classList.contains('hide')) {
    document.getElementById(element_id).classList.remove('hide');
  }
}

function loadCalendar() {
  if (month_offset <= 0) {
    hideElementWithId('button_kalender_fruher');
  } else {
    unveilElementWithId('button_kalender_fruher');
  }
  let text = '';
  if (BOOKING_PERIODS_LEFT === null && BOOKING_PERIODS_RIGHT === null) {
    hideElementWithId('button_kalender_fruher');
    hideElementWithId('button_kalender_spater');
    text += '<p><span class = "hinweis"><strong>Fehler:</strong> "' +
        occupancy_file_left_from_php + '" und "' +
        occupancy_file_right_from_php +
        '" konnten nicht gelesen werden.</span></p>';
  } else if (BOOKING_PERIODS_LEFT === null) {
    text += '<p><span class = "hinweis"><strong>Fehler:</strong> "' +
        occupancy_file_left_from_php +
        '" konnte nicht gelesen werden.</span></p>';
    text += loadCalendarFromBookingPeriods([BOOKING_PERIODS_RIGHT]);
  } else if (BOOKING_PERIODS_RIGHT === null) {
    text += '<p><span class = "hinweis"><strong>Fehler:</strong> "' +
        occupancy_file_right_from_php +
        '" konnte nicht gelesen werden.</span></p>';
    text += loadCalendarFromBookingPeriods([BOOKING_PERIODS_LEFT]);
  } else {
    text += loadCalendarFromBookingPeriods(
        [BOOKING_PERIODS_LEFT, BOOKING_PERIODS_RIGHT]);
  }

  document.getElementById('belegungskalender').innerHTML = text;
  return false;
}

function getNumberInvalidPeriods(booking_periods) {
  let number_invalid_periods = 0;
  for (let booking_period of booking_periods) {
    if (!booking_period.valid) {
      number_invalid_periods++;
    }
  }
  return number_invalid_periods;
}

function getInvalidBookingPeriodsText(booking_periods) {
  let number_invalid_periods = 0;
  for (let booking_periods_single_appartment of booking_periods) {
    number_invalid_periods +=
        getNumberInvalidPeriods(booking_periods_single_appartment);
  }
  text = '';
  if (number_invalid_periods > 0) {
    text += '<p><span class = "hinweis"><strong>Es konnten ' +
        number_invalid_periods +
        ' Buchungszeitr&auml;ume nicht gelesen werden.</strong></span></p>';
  }
  return text;
}

function loadCalendarFromBookingPeriods(bookings_periods) {
  let text = '';
  text += getInvalidBookingPeriodsText(bookings_periods);
  text += createCalendar(bookings_periods);
  return text;
}

function getFirstDateOfCalendar(offset) {
  let current_date = new Date();
  return new Date(
      current_date.getFullYear(), current_date.getMonth() + offset, 1, 0, 0, 0,
      0);
}

function getCalendarSettings() {
  let window_width = window.innerWidth;

  let settings = {number_months: 6, number_columns: 3};
  if (window_width < 650) {
    settings.number_months = 4;
    settings.number_columns = 2;
    if (window_width < 450) {
      settings.number_months = 2;
      settings.number_columns = 1;
    }
  }

  return settings;
}

function createCalendar(booking_periods) {
  let first_date_in_calendar = getFirstDateOfCalendar(month_offset);
  let settings = getCalendarSettings();

  let text = '<table id="kalender_tabelle_aussen">';
  text += '<tr>';
  for (let month_idx = 0; month_idx < settings.number_months; month_idx++) {
    let first_date_in_month = new Date(first_date_in_calendar);
    first_date_in_month.setMonth(first_date_in_month.getMonth() + month_idx);

    text += '<td class="kalender_tabelle_aussen_td">' +
        createMonth(first_date_in_month, booking_periods) + '</td>';
    if ((month_idx + 1) % settings.number_columns == 0) {
      text += '</tr><tr>';
    }
  }
  text += '</tr>';
  text += '</table>';

  return text;
}

const BookingStateSingleAppartment =
    {
      FREE: 0,
      BOOKED: 1,
      ARRIVAL: 2,
      DEPARTURE: 3
    }

function getBookingStateSingleAppartment(date, booking_periods) {
  for (let booking_period of booking_periods) {
    if (!booking_period.valid) {
      continue;
    }
    if (date.getTime() === booking_period.start_date.getTime()) {
      return BookingStateSingleAppartment.ARRIVAL;
    }
    if (date.getTime() === booking_period.end_date.getTime()) {
      return BookingStateSingleAppartment.DEPARTURE;
    }
    if (date.getTime() > booking_period.start_date.getTime() &&
        date.getTime() < booking_period.end_date.getTime()) {
      return BookingStateSingleAppartment.BOOKED;
    }
  }
  return BookingStateSingleAppartment.FREE;
}

function getBookingStateCellClass(date, booking_periods) {
  booking_states = [];
  for (let booking_periods_single_appartment of booking_periods) {
    booking_states.push(getBookingStateSingleAppartment(
        date, booking_periods_single_appartment))
  }

  let occupancies = 0;
  let arrivals = 0;
  let departures = 0;
  for (let booking_state of booking_states) {
    if (booking_state === BookingStateSingleAppartment.BOOKED) {
      occupancies += 1;
    } else if (booking_state === BookingStateSingleAppartment.ARRIVAL) {
      arrivals += 1;
    } else if (booking_state === BookingStateSingleAppartment.DEPARTURE) {
      departures += 1;
    }
  }

  let netto_arrivals = arrivals - departures;
  if (netto_arrivals === 0) {
    return 'class="calendar_occupancy_' + occupancies.toString() + '"';
  } else if (netto_arrivals > 0) {
    return 'class="calendar_arrival_' + occupancies.toString() + '_to_' +
        (occupancies + netto_arrivals).toString() + '"';
  } else if (netto_arrivals < 0) {
    return 'class="calendar_departure_' +
        (occupancies - netto_arrivals).toString() + '_to_' +
        occupancies.toString() + '"';
  }
}

function createMonthHeader(year, month) {
  const MONTH_NAMES = [
    'Januar', 'Februar', 'M&auml;rz', 'April', 'Mai', 'Juni', 'Juli', 'August',
    'September', 'Oktober', 'November', 'Dezember'
  ];
  const DAY_ABBREVIATIONS = ['Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa', 'So'];

  let text =
      '<tr><th colspan="7">' + MONTH_NAMES[month] + ' ' + year + '</th></tr>';
  text += '<tr>';
  for (let day of DAY_ABBREVIATIONS) {
    text += '<th>' + day + '</th>';
  }
  text += '</tr>';
  return text;
}

function getNumberDaysInMonth(year, month) {
  return new Date(year, month + 1, 0).getDate();
}

function getNumberEmptyPreCells(first_day_idx) {
  if (first_day_idx === 0) {
    return 6;
  }
  return first_day_idx - 1;
}

function createEmptyCells(numberEmptyCells) {
  let text = '';
  for (let idx = 0; idx < numberEmptyCells; idx++) {
    text += '<td>&nbsp;</td>';
  }
  return text;
}

function fillActualDates(first_date_in_month, booking_periods) {
  let text = '';

  let number_days = getNumberDaysInMonth(
      first_date_in_month.getFullYear(), first_date_in_month.getMonth());
  for (let day_offset = 0; day_offset < number_days; day_offset++) {
    let date = new Date(first_date_in_month);
    date.setDate(date.getDate() + day_offset);

    text += '<td ' + getBookingStateCellClass(date, booking_periods) + '>' +
        date.getDate() + '</td>';
    if (date.getDay() == 0) {  // Sunday
      text += '</tr><tr>';
    }
  }

  return text;
}

function fillMonthToUniqueSize(number_cells_created) {
  const CELLS_PER_MONTH = 42;
  let cells_to_create = CELLS_PER_MONTH - number_cells_created;
  let text = '';
  for (let idx = 0; idx < cells_to_create; idx++) {
    text += '<td>&nbsp;</td>';
    if ((number_cells_created + idx + 1) % 7 == 0) {
      text += '</tr><tr>';
    }
  }
  return text;
}

function createMonth(first_date_in_month, booking_periods) {
  let number_pre_cells = getNumberEmptyPreCells(first_date_in_month.getDay());
  let number_days = getNumberDaysInMonth(
      first_date_in_month.getFullYear(), first_date_in_month.getMonth());

  let text = '<table class="kalender_tabelle_innen">';
  text += createMonthHeader(
      first_date_in_month.getFullYear(), first_date_in_month.getMonth());
  text += '<tr>';
  text += createEmptyCells(number_pre_cells);
  text += fillActualDates(first_date_in_month, booking_periods);
  text += fillMonthToUniqueSize(number_pre_cells + number_days);
  text += '</tr>';
  text += '</table>';

  return text;
}

function createDate(date_string) {
  let numbers = date_string.split('.');
  let year = parseInt(numbers[2]);
  let month = parseInt(numbers[1]) - 1;
  let day = parseInt(numbers[0]);
  return new Date(year, month, day);
}

function isValidDate(date) {
  return !isNaN(date.getTime());
}

function createBookingPeriod(period_string) {
  let dates = period_string.split('-');
  if (dates.length != 2) {
    return null;
  }
  let start_date = createDate(dates[0]);
  let end_date = createDate(dates[1]);
  return {
    start_date: start_date,
    end_date: end_date,
    valid: isValidDate(start_date) && isValidDate(end_date)
  };
}

function extractBookingPeriodsFromRawInformations(raw_booking_informations) {
  if (raw_booking_informations === null) {
    return null;
  }

  let booking_periods = [];
  for (let x in raw_booking_informations) {
    let booking_period_string = new String(raw_booking_informations[x]);
    let booking_period = createBookingPeriod(booking_period_string);
    if (booking_period) {
      booking_periods.push(booking_period);
    }
  }

  booking_periods.sort(function(lhs, rhs) {
    return lhs.start_date - rhs.start_date;
  });

  return booking_periods;
}