<article class="inhalt_artikel">
	<?php
		global $anfrage_gesendet;
		if(!isset($anfrage_gesendet))
		{
			$anfrage_gesendet = 0;
		}
		// Originale Seite, noch nichts gesendet oder eins der Felder nicht ausgefuellt
		if($anfrage_gesendet == 0 || $anfrage_gesendet == 2)
		{
	?>
			<div id="inhalt_anfrage_links">
				<p>
					Die aktuelle Belegung entnehmen Sie unserem <a href="#belegungsplan">Belegungsplan</a>.
				</p>
				<form id="formular" method="post" action="index.php">
					<?php if($anfrage_gesendet == 2){echo'<p id="hinweis_oben">Bitte f&uuml;llen Sie alle Pflichtfelder aus, um das Formular abzuschicken.</p>';}?>
					<fieldset>
						<legend>
							Reise
						</legend>
						<ol class="formular_list">
							<li>
								<label for="formular_gast_datum_anreise" <?php if(($anfrage_gesendet == 2) && empty($_POST["gast_datum_anreise"])){echo 'class="hinweis"';}?>>Gew&uuml;nschtes Anreisedatum<sup>*</sup></label>
								<input id="formular_gast_datum_anreise" type="date" required name="gast_datum_anreise" <?php if(($anfrage_gesendet == 2) && !empty($_POST["gast_datum_anreise"])){echo 'value="'.$_POST["gast_datum_anreise"].'"';}?>/>
							</li>
							<li>
								<label for="formular_gast_datum_abreise" <?php if(($anfrage_gesendet == 2) && empty($_POST["gast_datum_abreise"])){echo 'class="hinweis"';}?>>Gew&uuml;nschtes Abreisedatum<sup>*</sup></label>
								<input id="formular_gast_datum_abreise" type="date" required name="gast_datum_abreise" <?php if(($anfrage_gesendet == 2) && !empty($_POST["gast_datum_abreise"])){echo 'value="'.$_POST["gast_datum_abreise"].'"';}?> />
							</li>
							<li>
								<label for="formular_gast_anzahl_personen" <?php if(($anfrage_gesendet == 2) && empty($_POST["gast_anzahl_personen"])){echo 'class="hinweis"';}?>>Anzahl Personen<sup>*</sup></label>
								<select id="formular_gast_anzahl_personen" required name="gast_anzahl_personen" <?php if(($anfrage_gesendet == 2) && !empty($_POST["gast_anzahl_personen"])){echo 'value="'.$_POST["gast_anzahl_personen"].'"';}?> >
									<option <?php if(($anfrage_gesendet == 0) || empty($_POST["gast_anzahl_personen"])){echo "selected";}?> disabled hidden value=''></option>
									<option <?php if(($anfrage_gesendet == 2) && !empty($_POST["gast_anzahl_personen"]) && ($_POST["gast_anzahl_personen"] == 1)){echo "selected";}?>>1</option>
									<option <?php if(($anfrage_gesendet == 2) && !empty($_POST["gast_anzahl_personen"]) && ($_POST["gast_anzahl_personen"] == 2)){echo "selected";}?>>2</option>
									<option <?php if(($anfrage_gesendet == 2) && !empty($_POST["gast_anzahl_personen"]) && ($_POST["gast_anzahl_personen"] == 3)){echo "selected";}?>>3</option>
									<option <?php if(($anfrage_gesendet == 2) && !empty($_POST["gast_anzahl_personen"]) && ($_POST["gast_anzahl_personen"] == 4)){echo "selected";}?>>4</option>
								</select>
							</li>
						</ol>
					</fieldset>
					<fieldset>
						<legend>
							Angaben zur Person
						</legend>
						<ol class="formular_list">
							<li>
								<label for="formular_gast_name" <?php if(($anfrage_gesendet == 2) && empty($_POST["gast_name"])){echo 'class="hinweis"';}?>>Name<sup>*</sup></label>
								<input id="formular_gast_name" type="text" required name="gast_name" <?php if(($anfrage_gesendet == 2) && !empty($_POST["gast_name"])){echo 'value="'.$_POST["gast_name"].'"';}?> />
							</li>
							<li>
								<label for="formular_gast_email" <?php if(($anfrage_gesendet == 2) && empty($_POST["gast_email"])){echo 'class="hinweis"';}?>>Email<sup>*</sup></label>
								<input id="formular_gast_email" type="email" required name="gast_email" <?php if(($anfrage_gesendet == 2) && !empty($_POST["gast_email"])){echo 'value="'.$_POST["gast_email"].'"';}?>/>
							</li>
							<li class="anfrage_email_bestaetigung">
								<label for="formular_gast_email_bestaetigung">Email Best&auml;tigung</label>
								<input id="formular_gast_email_bestaetigung" type="email" name="gast_email_bestaetigung" />
							</li>
							<li>
								<label for="formular_gast_telefon">Telefon&nbsp;</label>
								<input id="formular_gast_telefon" type="tel" name="gast_telefon" <?php if(($anfrage_gesendet == 2) && !empty($_POST["gast_telefon"])){echo 'value="'.$_POST["gast_telefon"].'"';}?>/>
							</li>
						</ol>
					</fieldset>
					<ol class="formular_list">
						<li>
							<label id="gast_nachricht_label" for="formular_gast_nachricht">Bitte teilen Sie uns Ihre Fragen und besonderen W&uuml;nsche mit</label>
							<textarea id="formular_gast_nachricht" name="gast_nachricht"><?php if(($anfrage_gesendet == 2) && !empty($_POST["gast_nachricht"])){echo $_POST["gast_nachricht"];}?></textarea>
						</li>
						<li>
							<input id="formular_gast_kopie" type="checkbox" name="gast_kopie" <?php if(($anfrage_gesendet == 2) && isset($_POST["gast_kopie"])){echo "checked";} ?> />
							<label for="formular_gast_kopie"><span></span>Eine Kopie der unverbindlichen Anfrage an mich senden</label>
						</li>
						<li>
							<input type="submit" name="anfrage_senden" value="Anfrage senden" class="button" />
							<p class="align_right" <?php if($anfrage_gesendet == 2){echo 'class="hinweis"';}?>>
								Felder mit <sup>*</sup> sind Pflichtfelder.
							</p>
						</li>
					</ol>
				</form>
			</div>
	<?php
		}
		// Nachricht versenden und Feedback an User
		else
		{			
			// Bots rauswerfen und deren Anzahl zaehlen
			if(!empty($_POST["gast_email_bestaetigung"])){
				$handle = fopen("log.txt","r");
				$numbots = fgets($handle);
				fclose($handle);
				$numbots++;
				$handle = fopen("log.txt","w");
				fwrite($handle, $numbots);
				fclose($handle);
			}
			else{
				// Feststellen von welcher Domain gesendet wurde
			 	$domain = (string) $_SERVER['HTTP_REFERER'];
			 	$buchstabe = substr($domain, 7, 1);
			 	if($buchstabe == "w") $buchstabe = substr($domain, 11, 1);
			 	if($buchstabe == "x"){
			 		$domain = "https://föhr-fewo.de";
			 	}
			 	else if($buchstabe == "f"){
			 		$domain = "https://ferienwohnung-nordseeinsel.de";
			 	}
			 	else{
			 		$domain = "https://urlaub-bei-friesen.de";
			 	}

				// Vorhandene Variablen auslesen
				if(isset($_POST["gast_datum_anreise"])){
					$datum_anreise = $_POST["gast_datum_anreise"];
				}
				if(isset($_POST["gast_datum_abreise"])){
					$datum_abreise = $_POST["gast_datum_abreise"];
				}
				if(isset($_POST["gast_anzahl_personen"])){
					$anzahl_personen = $_POST["gast_anzahl_personen"];
				}
				if(isset($_POST["gast_name"])){
					$name = $_POST["gast_name"];
				}
				if(isset($_POST["gast_email"])){
					$email = $_POST["gast_email"];
				}
				if(!empty($_POST["gast_telefon"])){
					$telefon = $_POST["gast_telefon"];
				}
				else{
					$telefon = "keine Angabe";
				}
				if(isset($_POST["gast_nachricht"])){
					$nachricht = $_POST["gast_nachricht"];
				}
				else{
					$nachricht = "";
				}
				if(isset($_POST["gast_kopie"])){
					$kopie = 1;
				}
				else{
					$kopie = 0;	
				}


				// Emailnachricht schreiben
				$email_inhalt = utf8_decode("Folgende Buchungsanfrage wurde für das Haus Lambertsen (".$domain.") gestellt:\n\n"."Name: ".$name."\nEmail: ".$email."\nTelefon: ".$telefon."\n\nGewünschte Anreise: ".$datum_anreise."\nGewünschte Abreise: ".$datum_abreise."\nAnzahl Personen: ".$anzahl_personen."\n\n\nPersönliche Nachricht von ".$name.":\n".$nachricht);
				$email_from1 = "From: ".$name."<".$email.">";
				$email_from2 = "From: Haus Lambertsen <lambertsen@borgsum.de>";
				// Email versenden
				if($kopie == 1){
					if(mail('lambertsen@borgsum.de', 'Kontaktanfrage im Haus Lambertsen', $email_inhalt, $email_from1) && mail($email, 'Kopie: Kontaktanfrage im Haus Lambertsen', $email_inhalt, $email_from2)){
						$email_gesendet = 1;
					}
					else{
						$email_gesendet = 0;
					}
				}
				if($kopie == 0){
					if(mail('lambertsen@borgsum.de', 'Kontaktanfrage im Haus Lambertsen', $email_inhalt, $email_from1)){
						$email_gesendet = 1;
					}
					else{
						$email_gesendet = 0;
					}
				}

				// Melde Gast, ob Vorgang erfolgreich
				if($email_gesendet == 1){
		?>
					<div id="inhalt_anfrage_links">
						<p>
							Die aktuelle Belegung entnehmen Sie unserem <a href="#belegungsplan">Belegungsplan</a>.
						</p>
						<p>
							Ihre Anfrage wurde erfolgreich versendet.
						</p>
						<p>
							Vielen Dank! Wir werden uns schnellstm&ouml;glich bei Ihnen melden.
						</p>
					</div>
		<?php
				}
				elseif($email_gesendet == 0){
		?>
					<div id="inhalt_anfrage_links">
						<p>
							Die aktuelle Belegung entnehmen Sie unserem <a href="#belegungsplan">Belegungsplan</a>.
						</p>
						<p>
							Ihre Anfrage konnte leider nicht versendet werden.
						</p>
						<p>
							Bitte versuchen Sie es noch einmal oder schreiben Sie uns direkt unter <a href="mailto:lambertsen@borgsum.de">lambertsen@borgsum.de</a> .
						</p>
					</div>
		<?php
				}
			}
		} 
	?>
	<div id="inhalt_anfrage_rechts">
		<div id="inhalt_anfrage_bild">
			<img style="width: 100%" src="anfrage/images/Arfst_und_Karin_Lambertsen.jpg" alt="Ihre Gastgeber: Arfst und Karin Lambertsen">
		</div>
		<p>
			Wir freuen uns auf Sie!
		</p>
		<p>Sie erreichen uns direkt via</p>
		<ul class="ul_pfeil">
			<li>Telefon: 04683-1092</li>
			<li>Email: <a href="mailto:lambertsen@borgsum.de">lambertsen@borgsum.de</a></li>
			<li>die Adresse: <p class="inhalt_adresse">
								Familie Lambertsen <br> S&uuml;&uuml;derwoi 5 <br> 25938 Borgsum
							</p></li>
		</ul>
	</div>
</article>

<article class="inhalt_artikel">
	<header>
		<a id="belegungsplan"></a>
		<h3>Belegungsplan</h3>
	</header>
	<noscript>
			<p class="hinweis">Der Kalender wird dynamisch mit Javascript erstellt. Um ihn zu betrachten, m&uuml;ssen Sie Javascript aktivieren.</p>
	</noscript>
	<div id="belegungskalender"></div>
	<div id="button_kalender_fruher"><button class="button">Fr&uuml;her</button></div>
	<div id="button_kalender_spater"><button class="button">Sp&auml;ter</button></div>
	<div id="kalender_legende">
		<table>
			<tr>
				<th><img src="anfrage/images/calendar/occupancy_0.png" alt="Wohnungen frei"></th>
				<td>Wohnungen frei</td>
				<th><img src="anfrage/images/calendar/arrival_0_to_1.png" alt="Anreisetag"></th>
				<td>Anreise</td>
			</tr>
			<tr>
				<th><img  src="anfrage/images/calendar/occupancy_1.png" alt="Eine Wohnung belegt"></th>
				<td>Eine Wohnung frei</td>
				<th><img src="anfrage/images/calendar/departure_1_to_0.png" alt="Abreisetag"></th>
				<td>Abreise</td>
			</tr>
			<tr>
				<th><img  src="anfrage/images/calendar/occupancy_2.png" alt="Wohnungen belegt"></th>
				<td>Wohnungen belegt</td>
				<th>&nbsp;</th>
				<td>&nbsp;</td>
			</tr>
		</table>
	</div>
</article>