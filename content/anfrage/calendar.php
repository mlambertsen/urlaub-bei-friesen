<?php
    $occupancy_file_left = "anfrage/config/belegung_links.txt";
    $occupancy_file_right = "anfrage/config/belegung_rechts.txt";
	$handle = fopen($occupancy_file_left, "r");
	if ($handle)
	{
		while (($line = fgets($handle)) !== false)
		{
			$raw_booking_informations_left_from_php[] = $line;
		}
		fclose($handle);
	}
	$handle = fopen($occupancy_file_right, "r");
	if ($handle)
	{
		while (($line = fgets($handle)) !== false)
		{
			$raw_booking_informations_right_from_php[] = $line;
		}
		fclose($handle);
	}

    echo "<script>";
    echo 'var occupancy_file_left_from_php = "'.$occupancy_file_left.'";';
	echo 'var occupancy_file_right_from_php = "'.$occupancy_file_right.'";';
    echo "var raw_booking_informations_left_from_php = ";
	if (isset($raw_booking_informations_left_from_php))
	{
		echo json_encode($raw_booking_informations_left_from_php).";";
	} else {
		echo "null;";
	}
	echo "var raw_booking_informations_right_from_php = ";
	if (isset($raw_booking_informations_right_from_php))
	{
		echo json_encode($raw_booking_informations_right_from_php).";";
	} else {
		echo "null;";
	}
	echo "</script>";
	echo '<script type="text/javascript" src="anfrage/scripts/calendar/calendar.js"></script>';

?>