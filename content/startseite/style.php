<?php
	header("Content-type: text/css");
?>

#text_zu_video
{
	float:left;
	width: 37%;
}

#youtube_video
{
	float: right;
	width: 60%;
	padding-left: 3%;
	height: 370px;
	height: 23.125rem;
	border: none;
}

#inhalt_services_links
{
	width: 45%;
	float: left;
	padding: 0 5% 0 0;
}

#inhalt_services_rechts
{
	width: 45%;
	float: left;
	padding: 0 0 0 5%;
}

#inhalt_services_links p, #inhalt_services_rechts p
{
	margin-top: 0;
}


/* Film-Teil für kleine Bildschirme */
@media (max-width: 674px)
{
	#text_zu_video
	{
		width: 100%;
	}

	#youtube_video
	{
		float: left;
		width: 100%;
		padding: 0;
	}
}


/* Service-Teil für kleine Bildschirme */
@media (max-width: 614px)
{
	#inhalt_services_links
	{
		width: 100%;
		padding: 0;
	}

	#inhalt_services_rechts
	{
		width: 100%;
		padding: 0;
	}
}