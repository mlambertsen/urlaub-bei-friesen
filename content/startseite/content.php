<header>
	<h1 class="ueberschrift">Herzlich Willkommen im Haus Lambertsen</h1>
</header>

<article class="inhalt_artikel">
	<header>
		<h3>Ein perfekter Urlaub auf F&ouml;hr</h3>
	</header>
	<div id="text_zu_video">
		<ul class="ul_checkmark">
			<li>Wohnen bei einheimischen Friesen</li>
			<li>zentrale Lage auf F&ouml;hr</li>
			<li>sehr ruhige Lage</li>
			<li>allergikergerecht</li>
			<li>Strandn&auml;he</li>
			<li>kinderfreundlich</li>
			<li>Parkplatz am Haus</li>
		</ul>
		<p>
			In unserem Youtube-Video erfahren Sie einiges über Ihr Feriendomizil.
		</p>
	</div>
	<iframe id="youtube_video" src="https://www.youtube.com/embed/HX2jQlYiXqs" allowfullscreen></iframe>
</article>

<article class="inhalt_artikel">
	<header>
		<h3>Unser Service</h3>
	</header>
	<div id="inhalt_services_links">
		<p>
			In unserem Angebot sind selbstverst&auml;ndlich kostenlos enthalten, damit Sie sich rundum wohl f&uuml;hlen k&ouml;nnen:
		</p>
		<ul class="ul_checkmark">
			<li>Bettw&auml;sche und Handt&uuml;cher</li>
			<li>Internetanschluss per W-LAN</li>
			<li>Kinderbett, Hochstuhl</li>
			<li>Abholservice und Einkaufsservice bei Anreise ohne Auto</li>
			<li>Ersteinkaufsservice</li>
			<li>Babysitterservice nach Absprache</li>
		</ul>
	</div>
	<div id="inhalt_services_rechts">
		<p>
			Das Wohnen bei Einheimischen h&auml;lt noch weitere Vorteile parat:
		</p>
		<ul class="ul_checkmark">
			<li>Ansprechpartner vor Ort</li>
			<li>Aktuelle Informationen und Tipps zu Ereignissen auf F&ouml;hr</li>
			<li>Erlebnis der friesischen Sprache</li>
		</ul>
	</div>
</article>