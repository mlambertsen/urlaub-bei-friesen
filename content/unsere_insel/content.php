<article class="inhalt_artikel">
	<header>
		<h3>Unser Dorf Borgsum</h3>
	</header>
	<div class="inhalt_borgsum_muehle">
		<img style="width: 100%" src="unsere_insel/images/Borgsumer_Muehle.jpg" alt="Borgsumer M&uuml;hle im Sommer, im Winter und bei Sonnenuntergang">
	</div>
	<p>
		Das Dorf Borgsum ist zentral auf der Insel gelegen. Es eignet sich ideal als Ausgangspunkt f&uuml;r Radtouren und Wanderungen &uuml;ber F&ouml;hr.
	</p>
	<p>
		Zudem befindet sich im Dorf die imposante Borgsumer M&uuml;hle, die ein beliebtes Ausflugsziel und Fotomotiv darstellt.
	</p>
	<p>
		Am Ortsrand von Borgsum befindet sich die Lembecksburg, ein Ringwall aus der Wikingerzeit. Von hier aus k&ouml;nnen sie &uuml;ber die gesamte Insel blicken und bei guter Sicht sogar die Nachbarinsel Amrum, einige Halligen und das Festland sehen.
	</p>
	<p>
		Borgsum hat einen Naturstrand, der an die Salzwiesen und den einzigen Fluss der Insel, die Godel, grenzt. Die Salzwiesen, ein wunderschönes Naturschutzgebiet mit einer vielfältigen Flora und Fauna, erreichen Sie wie auch den Strand in nur wenigen Gehminuten.
	</p>
	<div class="inhalt_borgsum_strand">
		<img style="width: 100%" src="unsere_insel/images/Borgsum_Strand.jpg" alt="Borgsumer Strand bei Ebbe und Flut">
	</div>
</article>

<article class="inhalt_artikel">
	<header>
		<h3>Die Insel F&ouml;hr</h3>
	</header>
	<p>
		Gesch&uuml;tzt durch die Inseln Amrum und Sylt herrscht hier ein angenehmes, mildes Reizklima, das zu jeder Jahreszeit heilkr&auml;ftig wirkt.
Klimaforscher haben festgestellt, dass die Insel mehr Sonnenstunden verbuchen kann als das nahegelegene Festland.
	</p>
	<p>
		Die Insel ist mit ungef&auml;hr 12km Länge und 8km Breite
82 km<sup>2</sup> groß und bietet ca. 8600 Einwohnern ein Zuhause. An dem 15km langen, flachabfallenden Sandstrand ist gefahrloses Baden m&ouml;glich.
	</p>
	<p>
		Die Insel beherbergt 16 h&uuml;bsche Friesend&ouml;rfer mit sch&ouml;nen reetgedeckten Friesenh&auml;usern, gem&uuml;tlichen Cafes, kleinen Gesch&auml;ften und Restaurants. Am F&auml;hrhafen hat sich als j&uuml;ngste Gemeinde der Insel die Stadt Wyk mit zahlreichen Einkaufsm&ouml;glichkeiten entwickelt.
	</p>
	<p>
		Das flache Land mit seinem guten Radwegenetz l&auml;dt zum Fahrradfahren ein. In der sch&ouml;nen Landschaft entdecken Sie Windm&uuml;hlen, historische Kirchen, Vogelkojen, Töpferstuben,  Museen und noch vieles mehr.
	</p>
	<p>
		Die nachfolgenden Listen stellen nur einen klitzekleinen Teil der M&ouml;glichkeiten auf F&ouml;hr dar. Eine umfassende(re) Liste stellen wir Ihnen in unseren Wohnungen zur Verf&uuml;gung.
	</p>
	<div class="inhalt_foehr_natur">
		<header>
			<h5>Natur</h5>
		</header>
		<ul class="ul_pfeil">
			<li>Weltnaturerbe "Nordfriesisches Wattenmeer"</li>
			<li>Vogelkojen</li>
			<li>Salzwiesen in Borgsum und Witsum</li>
			<li>Vogelschutzgebiete auf dem Vorland</li>
		</ul>
	</div>
	<div class="inhalt_foehr_sport">
		<header>
			<h5>Sport und Wellness</h5>
		</header>
		<ul class="ul_pfeil">
			<li>Schwimmen in der Nordsee oder im Wellenbad "Aqua F&ouml;hr"</li>
			<li>Kurmittelhaus mit Saunalandschaft, Thalassokur- und Wellnesszentrum</li>
			<li>Wattlaufen</li>
			<li>Fu&szlig;ball, Beachvolleyball, Tennis, Golf</li>
			<li>Windsurfen, Kiten, Segeln, Reiten</li>
		</ul>
	</div>
	<div class="inhalt_foehr_kultur">
		<header>
			<h5>Kultur</h5>
		</header>
		<ul class="ul_pfeil">
			<li>Historische Kirchen</li>
			<li>Museum Kunst der Westk&uuml;ste</li>
			<li>Friesen-Museum F&ouml;hr</li>
			<li>H&uuml;hnengr&auml;ber auf der ganzen Insel und die Lembecksburg in Borgsum</li>
		</ul>
	</div>
</article>

<article class="inhalt_artikel">
	<header>
		<h3>Das Nordfriesische Wattenmeer</h3>
	</header>
	<div class="inhalt_wattenmehr_sonnenuntergang">
		<img style="width: 100%" src="unsere_insel/images/Sonnenuntergang.jpg" alt="Sontenuntergang auf F&ouml;hr">
	</div>
	<p>
		Der Nationalpark Schleswig-Holsteinisches Wattenmeer wurde zusammen mit den anderen deutschen und dem niederl&auml;ndischen Wattenmeer Mitte 2009 zum UNESCO-Welt(natur)erbe erkl&auml;rt.
	</p>
	<p>
		Wer glaubt, schon alle Geheimnisse der Insel F&ouml;hr entdeckt zu haben, kann auf einem Tagesausflug die benachbarten Halligen oder die Inseln Sylt und Amrum erkunden.
	</p>
</article>