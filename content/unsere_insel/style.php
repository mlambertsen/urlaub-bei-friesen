<?php
	header("Content-type: text/css");
?>

.inhalt_borgsum_muehle
{
	float: right;
	width: 30%;
	padding-left: 3%;
}

.inhalt_borgsum_strand
{
	width: 100%;
	clear: both;
	padding-top: 15px;
	padding-top: 0.9375rem;
}

.inhalt_foehr_natur
{
	width: 31%;
	float: left;
	padding-right: 3%
}

.inhalt_foehr_sport
{
	width: 31%;
	float: left;
	padding-right: 3%
}

.inhalt_foehr_kultur
{
	width: 31%;
	float: left;
}

.inhalt_wattenmehr_sonnenuntergang
{
	float: right;
	width: 40%;
	padding-left: 3%;
}

@media (min-width: 600px) and (max-width: 779px)
{
	.inhalt_foehr_natur
	{
		width: 48%;
		padding-right: 3%;
	}

	.inhalt_foehr_sport
	{
		width: 48%;
		padding: 0;
	}

	.inhalt_foehr_kultur
	{
		width: 100%;
	}
}

@media (max-width: 599px)
{
	.inhalt_foehr_natur
	{
		width: 100%;
		padding: 0;
	}

	.inhalt_foehr_sport
	{
		width: 100%;
		padding: 0;
	}

	.inhalt_foehr_kultur
	{
		width: 100%;
	}
}