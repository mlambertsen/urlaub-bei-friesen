<?php require_once("preise_und_angebote/read_config.php"); ?>
<article class="inhalt_artikel">
	<header>
		<h3>Last Minute Angebot</h3>
	</header>
	<p>
		Zur Zeit haben wir keine Last Minute Angebote.
	</p>
</article>

<article class="inhalt_artikel">
	<header>
		<h3>Sonderangebot f&uuml;r Langzeiturlauber</h3>
	</header>
	<p>
		Sie m&ouml;chten 3 Wochen oder l&auml;nger bei uns verweilen? Dann erhalten Sie von uns den <span class = "hinweis"><strong>Langzeitrabatt</strong></span>. Sprechen Sie uns einfach darauf an, per Email, Telefon oder &uuml;ber unser <a href="index.php?seite=anfrage" title="Anfrage"><strong>Anfrageformular</strong></a>.
	</p>
</article>

<article class="inhalt_artikel">
	<header>
		<h3>Preistabelle (<?php echo get_prices_validity(); ?>)</h3>
	</header>
	<p>
		Die Preise gelten jeweils pro Wohnung und pro &Uuml;bernachtung bei einem Mindestaufenthalt von sieben N&auml;chten. Bei k&uuml;rzerer Verweildauer wird ein geringer Aufschlag erhoben.
	</p>
	<?php
        function print_prices_data_cells($prices, $season_key, $number_persons_key)
        {
            echo "<td>".$number_persons_key."</td>";
            echo "<td class='align_center'>".$prices[$season_key][$number_persons_key]."&euro;</td>";
        }
    ?>
	<table id="preistabelle">
		<tbody>
			<tr>
				<th></th>
				<th></th>
				<th>1 - 2 Personen</th>
				<th></th>
				<th>3 - 4 Personen</th>
			</tr>
			<tr>
				<td><strong>Nebensaison</strong><br class="table_break">Fr&uuml;hjahr und Herbst</td>
				<?php
				    print_prices_data_cells($prices, "Nebensaison", "1 - 2 Personen");
				    print_prices_data_cells($prices, "Nebensaison", "3 - 4 Personen");
				?>
			</tr>
			<tr>
				<td><strong>Zwischensaison</strong><br class="table_break">Ostern und Pfingsten</td>
				<?php
				    print_prices_data_cells($prices, "Zwischensaison", "1 - 2 Personen");
				    print_prices_data_cells($prices, "Zwischensaison", "3 - 4 Personen");
				?>
			</tr>
			<tr>
				<td><strong>Hauptsaison</strong><?php print_main_season_dates(); ?></td>
				<?php
				    print_prices_data_cells($prices, "Hauptsaison", "1 - 2 Personen");
				    print_prices_data_cells($prices, "Hauptsaison", "3 - 4 Personen");
				?>
			</tr>
		</tbody>
	</table>
	<p>
		Die aktuelle Belegung entnehmen Sie unserem <a href="index.php?seite=anfrage#belegungsplan">Belegungsplan</a>.
	</p>
</article>
