<?php
function get_prices_validity()
{
    $validity_file = "preise_und_angebote/config/validity.txt";
    $lines = file($validity_file);
    return $lines[0];
}

function print_main_season_dates()
{
    $main_season_file = "preise_und_angebote/config/main_season.txt";
    $main_season_date_ranges = file($main_season_file);
    $number_date_ranges = count($main_season_date_ranges);
    for ($i=0; $i < $number_date_ranges; ++$i) {
        echo "<br>".$main_season_date_ranges[$i];
    }
}

$prices_json_string = file_get_contents("preise_und_angebote/config/prices.json");
$prices = json_decode($prices_json_string, true);
?>