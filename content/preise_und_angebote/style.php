<?php
	header("Content-type: text/css");
?>

#preistabelle
{
	line-height: 1.5rem;
	display: block;
}

@media (min-width: 520px)
{
	td, th
	{
		padding-left: 1rem;
		padding-right: 1rem;
		border-width: 3px;
		border-width: 0.1875rem;
		border-style: outset;
	}

	th:first-of-type
	{
		visibility: hidden;
	}

	td:nth-of-type(2), td:nth-of-type(4), th:nth-of-type(2), th:nth-of-type(4)
	{
		display: none;
	}

	.th_links_klein
	{
		display: none;
	}
}


@media (max-width: 519px)
{
	table, tbody, th, td, tr
	{
		display: block;
	}

	tr:first-of-type
	{
		display: none;
	}

	.table_break
	{
		display: none;
	}

	td:first-of-type
	{
		padding: 10px 0 3px 0;
		padding: 0.625rem 0 0.1875rem 0;
		clear: both;
	}

	td:nth-of-type(2), td:nth-of-type(4)
	{
		padding-left: 3%;
		border-width: 3px;
		border-width: 0.1875rem;
		border-style: outset;
		width: 60%;
		width: calc(67% - 15px);
		width: calc(67% - 0.9375rem);
		float: left;
	}

	td:nth-of-type(3), td:nth-of-type(5)
	{
		border-width: 3px;
		border-width: 0.1875rem;
		border-style: outset;
		width: 30%;
		float: left;
	}
}